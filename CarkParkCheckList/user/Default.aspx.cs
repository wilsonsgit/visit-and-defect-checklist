﻿using CarkParkCheckList.Models;
using CarParkCheckList.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarkParkCheckListUser
{
    public partial class _Default : System.Web.UI.Page
    {
        private DBHelperUser db;
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            db = new DBHelperUser();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           

        }
        public void getTableString()
        {
            List<User> userList = db.getUserList(Session["SITE_CODE"].ToString());
            string str = "";
            foreach (User user in userList){
                str += "<tr>" +
                        "<td>&nbsp;</td>" +
                        "<td>" + user.username + "</td>" +
                        "<td>" + user.lastdatetimelogin + "</td>" +
                        "<td>" + user.islock + "</td>" +
                        "</tr>";
            }
            Response.Write(str);
        }
        
    }
}