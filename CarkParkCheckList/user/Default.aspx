﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CarkParkCheckListUser._Default" MasterPageFile="~/CarParkCheckList.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="bodyTitle" runat="server">
     <h3 class="page-header"><span class="text-primary">Manage User</span></h3>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server" class="field-container">
     <div class="container-fluid">
        <div class="row">
            <div class="panel-body">
                <a href="./add/" class="btn btn-success" role="button">Add New User</a>
            </div>
            <div class="col-lg-12">
                <div class="intro-message">
                    <table id="listContainer" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
                            <th>Action</th>
							<th>User Name </th>
							<th>Last Login</th>
                            <th>Status</th>
						</tr>
					</thead>
				
					<tbody>
                        <%getTableString();%>                        
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>

</asp:Content>