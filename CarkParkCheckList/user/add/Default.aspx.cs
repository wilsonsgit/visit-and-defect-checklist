﻿using CarParkCheckList.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarkParkCheckListUserAdd
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DBHelperUser db = new DBHelperUser();
            string siteid = Session["SITE_CODE"].ToString();
            txtUserGrp.DataSource = db.getUserGroup(siteid);
            txtUserGrp.DataTextField = "DESCRIPTION";
            txtUserGrp.DataValueField = "ROLEID";
            txtUserGrp.DataBind();
            db.closeSQLConn();
        }
    }
}