﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CarkParkCheckListUserAdd._Default" MasterPageFile="~/CarParkCheckList.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="bodyTitle" runat="server">
    <h3 class="page-header">
        <a href="../">Manage User </a>
        <span class="fa fa-angle-double-right fa-fw text-muted"></span>
        <span class="text-primary">Add User</span>
    </h3>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server" class="field-container">
     <div class="container-fluid col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading">
                <strong>New User Form</strong>
            </div>
            <div class="panel-body ">
                <div class="row  col-md-6">
                    <div class="form-group input-group ">
                        <span class="input-group-addon"><strong>@</strong></span>
                        <input type="text" class="form-control" placeholder="Wilson User Email" runat="server" id="txtUsername"/>
                        <span class="input-group-addon"><strong>@wilsonparking.com.sg</strong></span>
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-group"></i></span>
                        <select runat="server"   id="txtUserGrp" class="demo-default txtSelect" placeholder="User Group" required />
                         <span class="input-group-addon"><a href="#"><i class="fa fa-plus"></i></a></span>
                    </div>
                    <div class="form-group input-group">
                        <button type="reset" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>