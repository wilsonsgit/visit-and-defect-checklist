﻿using System;
using System.Web.UI;
using CarkParkCheckList.Utility;
using CarkParkCheckList.Models;
using CarParkCheckList.Controllers;
using System.Security.Cryptography;
using System.Xml;

namespace CarkParkCheckList
{
    public partial class _Default : System.Web.UI.MasterPage
    {

        private string SITE_CODE = "CPCHKLIST";

        protected void Page_PreLoad(object sender, EventArgs e)
        {

        }
     
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["SITE_CODE"] = SITE_CODE;
            Page.Title = "Car Park Checklist";
            if (Request.QueryString["auth"] != null)
            {
                UserSession sess;
                string auth = Request.QueryString["auth"];
                MD5 md5Hash = MD5.Create();

                string SESS_TOKEN = new MD5Hash().GetMd5Hash(md5Hash, String.Concat(auth,Session["SITE_ID"]));
                Session["SITE_TOKEN"] = SESS_TOKEN;
                Session["SITE_AUTH"] = auth;
                CarParkController cpc = new CarParkController();
                sess = cpc.getUserToken(SESS_TOKEN);

                if (sess != null)
                {
                    User user = new User();
                    user.username = sess.RequestBy;

                    new SessionManager().loginUser(user);
                }
          
            }

           
            if (Request.QueryString["l"] != null)
            {
                logout();
            }

            if (!new SessionManager().isLogIn())
            {
                Session["SITE_ID"] = Session.SessionID;
                Response.Redirect(getLoginUrl() + "?service=" + SITE_CODE + "&continue=" + getCurrentUrl() + "&rid=" + Session["SITE_ID"]);
            }

            User cuser = (User) Session["CURRENT_USER_OBJ"];
            txtUserName.Text = cuser.username;
        }

        private void logout()
        {
            new SessionManager().logout();
            Session["SITE_ID"] = Session.SessionID;
            Response.Redirect(getLoginUrl() + "?service=" + SITE_CODE + "&continue=" + getCurrentUrl() + "&rid=" + Session["SITE_ID"]);
        }

        private string getCurrentUrl()
        {
            string str = Request.Url.AbsoluteUri;
            string[] arr = str.Split('?');
            if (arr.Length > 0)
            {
                return arr[0];
            }
            return str;
        }

        private string getLoginUrl(){
            string url="";
            XmlDocument root = new XmlDocument();
            root.Load( Server.MapPath("~/config.xml.dat"));
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(root.NameTable);
            nsmgr.AddNamespace("cg", "urn:site-config");
            XmlNode node = root.SelectSingleNode("descendant::cg:config[cg:sites-req]", nsmgr);
            url = node.FirstChild.InnerText;
            return url;
        }
    }
}