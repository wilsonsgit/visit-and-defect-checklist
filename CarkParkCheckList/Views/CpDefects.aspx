﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Site1.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form>
        <div class="form-group">
            <div class="panel-group" id="accordion">
                <div  class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                               General Car Park Surrounding
                            </h5>
                        </div>
                        <div class="panel-body">
                            <div  class="col-md-4">
                                <label for="email">Operation Executive</label>
                                <asp:TextBox ID="txtOperationExacutive" type="text" class="form-control"  runat="server"></asp:TextBox>
                            </div>
                            <div  class="col-md-4">
                                <label for="pwd">Site</label>
                                <asp:TextBox ID="txtSite" type="text" class="form-control"  runat="server"></asp:TextBox>
                            </div>
                            <div  class="col-md-4">
                                <label class="control-label" for="date">Date</label>
                                <input class="form-control" id="txtDate" name="date" placeholder="DD/MM/YYY" type="text"/>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse1">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col1" class="glyphicon glyphicon-chevron-down"></span></span>
                                    General Car Park Surrounding
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label1" class="control-label" runat="server" Text="Bollards"></asp:Label>
                                    <asp:TextBox ID="txtBollards" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label2" class="control-label" runat="server" Text="Equipment Cleanliness"></asp:Label>
                                    <asp:TextBox ID="txtEquipmentCleanliness" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label3" class="control-label" runat="server" Text="Sign"></asp:Label>
                                    <asp:TextBox ID="txtSign" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label4" class="control-label" runat="server" Text="Spot Light"></asp:Label>
                                    <asp:TextBox ID="txtSpotLight" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label5" class="control-label" runat="server" Text="Light-Box"></asp:Label>
                                    <asp:TextBox ID="txtLightBox" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label6" class="control-label" runat="server" Text="Scrap Barrier"></asp:Label>
                                    <asp:TextBox ID="txtScrapBarrier" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label7" class="control-label" runat="server" Text="Safety Concerns"></asp:Label>
                                    <asp:TextBox ID="txtSafetyConcerns" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label8" class="control-label" runat="server" Text="Car Park Lights"></asp:Label>
                                    <asp:TextBox ID="txtCarParkLights" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label9" class="control-label" runat="server" Text="Pot Holes"></asp:Label>
                                    <asp:TextBox ID="txtPotHoles" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <asp:Label ID="Label10" class="control-label" runat="server" Text="General Cleanliness"></asp:Label>
                                    <asp:TextBox ID="txtGeneralCleanliness" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-6">
                                    <asp:Label ID="Label11" class="control-label" runat="server" Text="Water Pounding"></asp:Label>
                                    <asp:TextBox ID="txtWaterPounding" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <asp:Label ID="Label12" class="control-label" runat="server" Text="Rate Board / T&C / Beware of Barrier / Side Panel Sticker / Hotline No / Season Enquires"></asp:Label>
                                    <textarea class="form-control" cols="12" rows="5" id="txtComment"></textarea>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse2">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col2" class="glyphicon glyphicon-chevron-down"></span></span>
                                     Car Park Lots
                                </a>
                             </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label13" class="control-label" runat="server" Text="Faded Paint Works"></asp:Label>
                                    <asp:TextBox ID="txtFadedPaintWorks" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label14" class="control-label" runat="server" Text="Oil Leak"></asp:Label>
                                    <asp:TextBox ID="txtOilLeak" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label15" class="control-label" runat="server" Text="Water Pounding"></asp:Label>
                                    <asp:TextBox ID="txtWaterPoundingCP" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <asp:Label ID="Label16" class="control-label" runat="server" Text="PGS System"></asp:Label>
                                    <asp:TextBox ID="txtPGSSystem" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-6">
                                    <asp:Label ID="Label17" class="control-label" runat="server" Text="Damage Surface"></asp:Label>
                                    <asp:TextBox ID="txtDamageSurface" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse3">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col3" class="glyphicon glyphicon-chevron-down"></span></span>
                                    Hardware
                                </a>
                             </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label18" class="control-label" runat="server" Text="Scanning Issues"></asp:Label>
                                    <asp:TextBox ID="txtScanningIssues" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label19" class="control-label" runat="server" Text="Card Reader"></asp:Label>
                                    <asp:TextBox ID="txtCardReader" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label20" class="control-label" runat="server" Text="CCTV"></asp:Label>
                                    <asp:TextBox ID="txtCCTV" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <asp:Label ID="Label21" class="control-label" runat="server" Text="System Kiosk"></asp:Label>
                                    <asp:TextBox ID="txtSystemKiosk" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-6">
                                    <asp:Label ID="Label22" class="control-label" runat="server" Text="Barrier Kiosk"></asp:Label>
                                    <asp:TextBox ID="txtBarrierKiosk" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <asp:Label ID="Label23" class="control-label" runat="server" Text="Barrier Padding / Barrier Arm "></asp:Label>
                                    <asp:TextBox ID="txtBarrierPaddingArm" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-6">
                                    <asp:Label ID="Label24" class="control-label" runat="server" Text="Cable Tie"></asp:Label>
                                    <asp:TextBox ID="txtCableTie" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"  data-toggle="collapse" href="#collapse4">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col4" class="glyphicon glyphicon-chevron-down"></span></span>
                                    Car Park Booth
                                </a>
                             </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <asp:Label ID="Label25" class="control-label" runat="server" Text="Aircon & Drainage Pipe"></asp:Label>
                                    <asp:TextBox ID="txtAirconDrainaigePipe" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-6">
                                    <asp:Label ID="Label26" class="control-label" runat="server" Text="Booth Condition"></asp:Label>
                                    <asp:TextBox ID="txtBoothCondition" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <asp:Label ID="Label27" class="control-label" runat="server" Text="Booth Flooring"></asp:Label>
                                    <asp:TextBox ID="txtBoothFlooring" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-6">
                                    <asp:Label ID="Label28" class="control-label" runat="server" Text="Booth Wrap"></asp:Label>
                                    <asp:TextBox ID="txtBoothWrap" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse5">
                                <a>
                                 <span class="pull-right"><span id="Glyp-Col5" class="glyphicon glyphicon-chevron-down"></span></span>
                                Any Other Deffects</a>
                             </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-12">
                                   <asp:Label ID="Label29" class="control-label" runat="server" Text="Remarks"></asp:Label>
                                    <textarea class="form-control" rows="5" id="txtRemarks"></textarea>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script language="JavaScript" type="text/javascript">
        $('#collapse1').on('show.bs.collapse', function () {
            $('#Glyp-Col1').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
        });
        $('#collapse1').on('hide.bs.collapse', function () {
            $('#Glyp-Col1').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
        });
        $('#collapse2').on('show.bs.collapse', function () {
            $('#Glyp-Col2').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
        });
        $('#collapse2').on('hide.bs.collapse', function () {
            $('#Glyp-Col2').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
        });
        $('#collapse3').on('show.bs.collapse', function () {
            $('#Glyp-Col3').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
        });
        $('#collapse3').on('hide.bs.collapse', function () {
            $('#Glyp-Col3').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
        });
        $('#collapse4').on('show.bs.collapse', function () {
            $('#Glyp-Col4').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
        });
        $('#collapse4').on('hide.bs.collapse', function () {
            $('#Glyp-Col4').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
        });
        $('#collapse5').on('show.bs.collapse', function () {
            $('#Glyp-Col5').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
        });
        $('#collapse5').on('hide.bs.collapse', function () {
            $('#Glyp-Col5').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
        });
    </script>
</asp:Content>
