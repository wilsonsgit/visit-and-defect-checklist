﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CarkParkCheckListStatuf._Default" MasterPageFile="~/CarParkCheckList.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="bodyTitle" runat="server">
    <h3 class="page-header">
        <a href="../">Statistician </a>
        <span class="fa fa-angle-double-right fa-fw text-muted"></span>
        <span class="text-primary">User v Form</span>
    </h3>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server" class="field-container">
     <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
                <div class="intro-message">
                    <table id="listContainer" class="display stat_table" cellspacing="0" width="100%">
					<thead>
						<% getHeaderString(); %>
					</thead>
				
					<tbody>
                        <% getTableString(); %>                        
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>