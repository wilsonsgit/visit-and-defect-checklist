﻿using CarParkCheckList.Controllers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarkParkCheckListStatuf
{
    public partial class _Default : System.Web.UI.Page
    {
        private DBHelperStatuf db;
        private SqlDataReader sqlData; 
        protected void Page_Load(object sender, EventArgs e)
        {
            db = new DBHelperStatuf();
            sqlData = db.getStat();
        }

        
        public void getHeaderString(){

            String str = "<tr><th>User</th>";
            for(int i=0;i<sqlData.FieldCount;i++)
            {
                if(i>=2)
                    str += "<th>" + (i-1) + "</th>";
            }
            str += "</tr>";
            Response.Write(str);
        }
        public void getTableString()
        {
            string  str = "";
            string res = "";
            while (sqlData.Read())
            {
              
                str = "<tr>";
                for (int i = 0; i < sqlData.FieldCount; i++)
                {
        
                    if (i == 0)
                    {
                        str += "<td>" + sqlData.GetString(i) + "</td>";
                    }
                    else if (i > 1)
                    {
                        str += "<td>" + sqlData[sqlData.GetName(i)].ToString() + "</td>";
                    }

                }
                str += "</tr>";
                res += str;
            }
            
            Response.Write(res);
        }
    }
}