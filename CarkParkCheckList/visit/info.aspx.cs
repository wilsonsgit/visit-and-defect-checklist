﻿using CarParkCheckList.Controllers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkCheckList.Models;
using System.Globalization;
using CarParkCheckList.Utility;
using CarkParkCheckList.Utility;
using System.IO;

namespace CarkParkCheckList.VisitInfo
{
    public partial class _Default : System.Web.UI.Page
    {
        private static String IMG_PATH = new Constant().VISIT_IMG_LOC();
        public Visit visit;
        private AES aes;
        private CarParkController carParkController;
        protected String editLink;
        protected String copyLink;
        protected String deleteLink;

        protected void Page_Load(object sender, EventArgs e)
        {
            visit = new Visit();
            aes = new AES();   
            carParkController = new CarParkController();
            if (Request["q"] != null && !String.IsNullOrEmpty(Request["q"]))
            {
                string qid = aes.decrypt(Server.HtmlDecode(Request["q"]));
                string id = Request["q"];
                string[] mode = { Server.UrlEncode(aes.encrypt("MODE_1")), Server.UrlEncode(aes.encrypt("MODE_2")) };
                editLink = "./form.aspx?m=" + mode[0] + "&q=" + id;
                copyLink = "./form.aspx?m=" + mode[1] + "&q=" + id;
                deleteLink = "onclick=deleteView('" + id + "')";
                populateData(qid);
            }
        }


        private void populateData(String id) 
        {
            CustomListItem cli = new CustomListItem();
            visit = carParkController.getVisitDataByID(id);
            if (Session["CURRENT_USER"].Equals(visit.operationExec))
                infoAction.Visible = true;
            carParkController.closeSQLConn();

           visit.intercom = cli.getIntercomSelection()[visit.intercom];
           visit.equipmentCleanliness = cli.getEqpCleanlSelection()[visit.equipmentCleanliness];
           visit.receiptChecks = cli.getReceiptChecksSelection()[visit.receiptChecks];
           visit.recordingStatus = cli.getRecStatSelection()[visit.recordingStatus];
           visit.timeSynPms = cli.getTimeSyncPMSSelection()[visit.timeSynPms];
           getImages(id);

            
        }

        private void getImages(String id)
        {
            String path = IMG_PATH + id + "/";
            Image[] img = { image1, image2, image3};
            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
            if (exists)
            {
                string[] fileEntries = Directory.GetFiles(Server.MapPath(path));

                for (int i = 0; i < fileEntries.Length; i++)
                {

                    String filenameRelative = path + Path.GetFileName(fileEntries[i]);
                    img[i].Visible = true;
                    img[i].ImageUrl = ResolveClientUrl(filenameRelative);
                }
            }
        }

        public void _out(String str)
        {
            Response.Write(str);
        }
    }
}