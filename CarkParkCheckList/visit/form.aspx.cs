﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using CarParkCheckList.Models;
using CarParkCheckList.Controllers;
using CarParkCheckList.Utility;
using System.Data;
using System.Web.UI.HtmlControls;
using CarkParkCheckList.Utility;
using System.IO;
using System.Security.AccessControl;

namespace CarParkCheckList.VisitForm
{
    public partial class _Default : System.Web.UI.Page
    {
        private static String IMG_PATH = new Constant().VISIT_IMG_LOC();
        private DateTime dtDate;
        public String formId;
        public String mode;
        private AES aes;
        private CarParkController carParkController;
        private Visit currentVisitData;
       

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            aes = new AES();
            carParkController = new CarParkController();
            formId = " (New Application)";
            if (Request.QueryString["q"] != null && Request.QueryString["m"] != null)
            {
                try { 
                    formId = aes.decrypt(Server.HtmlDecode(Request["q"]));
                    mode = aes.decrypt(Server.HtmlDecode(Request["m"]));

                    if (mode == "MODE_1")
                    {
                        Session["INFO_MSG"] = new Constant().dangerMessage("Form is now open for changes.");
                        Session["INFO_LOC_MSG"] = "Edit Form";
                    }
                    else
                    {
                        Session["INFO_MSG"] = new Constant().infoMessage("Form has been copied.");
                        Session["INFO_LOC_MSG"] = "Copy Form";
                       
                    }
                    
                    currentVisitData = carParkController.getVisitDataByID(formId);
                    if (!IsPostBack) 
                    {
                        initSelector();
                        initFieldsContent(currentVisitData);
                        if(mode == "MODE_2")
                            initSpecial();
                    }
                    
                }
                catch
                {
                    Response.Redirect("./");
                }
            }
            else
            {
                if (!IsPostBack) 
                { 
                    initSpecial();
                    initSelector();
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "Car Park Visit Form";
        }


        private void initSpecial()
        {
            txtDate.Text = System.DateTime.Now.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            txtTime.Text = System.DateTime.Now.ToString("HH:mm", CultureInfo.InvariantCulture);
            txtOperationExacutive.Text = (string) Session["CURRENT_USER"];
        }
        private void initSelector()
        {
            CustomListItem listItem = new CustomListItem();

            DataSet ds = new CarParkController().getCpcodeDataSet();
            txtSite.DataSource = ds;
            txtSite.DataTextField = "CPCODE";
            txtSite.DataValueField = "CPCODE";
            txtSite.DataBind();


            dataBind(txtIntercom, listItem.getIntercomSelection());
            dataBind(txtEquipmentCleanliness, listItem.getEqpCleanlSelection());
            dataBind(txtRecordingStat, listItem.getRecStatSelection());
            dataBind(txtTimeSynPMS, listItem.getTimeSyncPMSSelection());
            dataBind(txtReceiptChecks, listItem.getReceiptChecksSelection());
            
            
            
         
        }

        private void dataBind(DropDownList dl, Dictionary<String, String> dic)
        {
            dl.DataSource = dic;
            dl.DataTextField = "Value";
            dl.DataValueField = "Key";
            dl.DataBind();
        }

        private void initFieldsContent(Visit visit)
        {

            dtDate = DateTime.ParseExact(visit.date, "dd MMM yyyy", CultureInfo.InvariantCulture);
            txtDate.Text = dtDate.ToString("MM/dd/yyyy");
            dtDate = DateTime.Parse(visit.time, CultureInfo.InvariantCulture);
            txtTime.Text = dtDate.ToString("HH:mm");

            txtSite.SelectedValue = visit.site;
            txtOperationExacutive.Text = visit.operationExec;
            txtCarInPark.Text = visit.carInPark;
            txtIntercom.SelectedValue = visit.intercom;
            txtSpareBarrierArm.Text = visit.spareBarrierArm;
            txtBuildingTenancy.Text = visit.buildingTenancy;
            txtEquipmentCleanliness.SelectedValue = visit.equipmentCleanliness;
            txtCopBook.Text = visit.copBookNo;
            txtSeasonTerm.Text = visit.seasonTerminations;
            txtNewSeasonApp.Text = visit.newSeasonTerminations;
            txtRefundForms.Value = visit.refundForms;
            txtcctv.Value = visit.cctvCamera;
            txtRecordingStat.SelectedValue = visit.recordingStatus;
            txtTimeSynPMS.SelectedValue = visit.timeSynPms;
            txtEarliestDateFootage.Text = visit.earliestDateOfFootage;
            txtTotalBarRaise.Text = visit.totalBarrierRaise;
            txtReceiptChecks.SelectedValue = visit.receiptChecks;
            txtContainersOnLocation.Text = visit.containersOnLoc;
            txtAbandonedVehicle.Value = visit.abandVehicle;
            txtSurroundinChanges.Value = visit.surroundingChanges;
            txtAnyOther.Value = visit.remarks;

            getImages(visit.jobid);
        }

        private void getImages(String id)
        {
            String path = IMG_PATH + id + "/";
            HtmlInputFile[] imgInput = { imageInput1, imageInput2, imageInput3 };
            HtmlGenericControl[] imgView = { imageInput1_view, imageInput2_view, imageInput3_view };
            Image[] imgtag = { image1, image2, image3 };
            
            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
            if (exists)
            {

                string[] fileEntries = getImagesLocation(id);

                for (int i = 0; i < fileEntries.Length; i++)
                {
                    imgView[i].Visible = true;
                    String filenameRelative = path + Path.GetFileName(fileEntries[i]);
                    imgtag[i].ImageUrl = ResolveClientUrl(filenameRelative);
                }
            }
        }

        private string[] getImagesLocation(String id)
        {
            string[] fileEntries = null;
            String path = IMG_PATH + id + "/";
            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
            if(exists)
                fileEntries = Directory.GetFiles(Server.MapPath(path));

            return fileEntries;
        }

        protected void btnListener(object sender, EventArgs e)
        {
            //MODE_1 = EDIT; MODE_2 = COPY/NEW;
            Visit visit = new Visit();
            bool IS_UPDATE = false;
            Session["INFO_MSG"] = new Constant().successMessage("Data has been saved.");
            if (mode == "MODE_1")
            {
                 IS_UPDATE = true;
                 Session["INFO_MSG"] = new Constant().successMessage("Data has been updated.");
            }

            try
            {
                Object insertedDataId=null;
                visit = getFormData(IS_UPDATE);
                insertedDataId = new CarParkController().saveCarparkVisits(visit, IS_UPDATE);

                if (insertedDataId != null)
                {
                    createdImgFolder(insertedDataId);
                }
                if (IS_UPDATE)
                {
                    updateImage(visit.jobid);
                }
                
            }
            catch (Exception)
            {

                Session["INFO_MSG"] = new Constant().warningMessage("An error occur while saving your form, please check your data/internet connection.");
            }
            Response.Redirect("../home.aspx");       
        }

        private void updateImage(String id)
        {
            String[] imagesLoc = getImagesLocation(id);
            HtmlInputFile[] imgInput = { imageInput1, imageInput2, imageInput3 };
            HiddenField[] dlist = { d1, d2, d3 };


            for(int i=0; i< dlist.Length; i++){
                if(dlist[i].Value.Equals("1"))
                {
                    File.Delete(ResolveClientUrl(imagesLoc[i]));
                }
            }
            createdImgFolder(id);
        }

        private void createdImgFolder(Object fname)
        {
            String path = IMG_PATH + fname;
            bool exists = System.IO.Directory.Exists(Server.MapPath(path));

            if (!exists)
                System.IO.Directory.CreateDirectory(Server.MapPath(@path));

            uploadImage(fname, imageInput1);
            uploadImage(fname, imageInput2);
            uploadImage(fname, imageInput3);
        }

        private void uploadImage(Object path,HtmlInputFile file)
        {
            var loc = Server.MapPath(@IMG_PATH + path + "/" + file.PostedFile.FileName);
            try
            {
                file.PostedFile.SaveAs(loc);
            }
            catch (Exception) { }
        }

      
      
        private Visit getFormData(bool isUpdate)
        {
            Visit visit = new Visit();
            if (!isUpdate)
            {
                dtDate = DateTime.ParseExact(txtDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                visit.date = dtDate.ToString("yyyy-MM-dd");
                visit.operationExec = txtOperationExacutive.Text;
                visit.time = txtTime.Text;
                visit.createdby = (String)Session["CURRENT_USER"];
            }
            else
            {
                visit.jobid = currentVisitData.jobid;
            }

            visit.site = txtSite.SelectedValue;
            visit.carInPark = txtCarInPark.Text;
            visit.intercom = txtIntercom.SelectedValue;
            visit.spareBarrierArm = txtSpareBarrierArm.Text;
            visit.buildingTenancy = txtBuildingTenancy.Text;
            visit.equipmentCleanliness = txtEquipmentCleanliness.SelectedValue;
            visit.copBookNo = txtCopBook.Text;
            visit.seasonTerminations = txtSeasonTerm.Text;
            visit.newSeasonTerminations = txtNewSeasonApp.Text;
            visit.refundForms = txtRefundForms.Value;
            visit.cctvCamera = txtcctv.Value;
            visit.recordingStatus = txtRecordingStat.SelectedValue;
            visit.timeSynPms = txtTimeSynPMS.SelectedValue;
            visit.earliestDateOfFootage = txtEarliestDateFootage.Text;
            visit.totalBarrierRaise = txtTotalBarRaise.Text;
            visit.receiptChecks = txtReceiptChecks.SelectedValue;
            visit.containersOnLoc = txtContainersOnLocation.Text;
            visit.abandVehicle = txtAbandonedVehicle.Value;
            visit.surroundingChanges = txtSurroundinChanges.Value;
            visit.remarks = txtAnyOther.Value;
            visit.updatedby = (String)Session["CURRENT_USER"];
            visit.lastupdate = System.DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            return visit;
        } 
    }
}