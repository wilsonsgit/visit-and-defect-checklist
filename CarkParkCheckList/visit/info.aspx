﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="info.aspx.cs" Inherits="CarkParkCheckList.VisitInfo._Default" MasterPageFile="~/CarParkCheckList.Master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="bodyTitle" runat="server">
       <h3 class="page-header">
           <span class="text-muted">Check List</span>
           <span class="fa fa-angle-double-right fa-fw text-muted"></span>
           <span class="text-muted">Visit</span>
            <span class="fa fa-angle-double-right fa-fw text-muted"></span>
           <span class="text-primary">Veiw Form</span>
       </h3>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server">
    <div class="container col-md-12">
        <div class="panel-body"> 
            <a href="./" class="btn btn-info" role="button"><span class='glyphicon glyphicon-menu-left'></span>&nbsp;Back</a>
            <asp:panel class="btn-group pull-right" Visible="false" ID="infoAction" runat="server">
                <a href='<%Response.Write(copyLink);%>' class="btn btn-success">Copy</a>
                <a href='<%Response.Write(editLink);%>' class="btn btn-warning">Edit</a>
                <a href='#' class="btn btn-danger" <%Response.Write(deleteLink);%>>Delete</a>
            </asp:panel>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Car Park Visit Details </h4> 
            <div class="panel-body">
                 <div  class="col-md-3">
                    <div class="input-group">
                        <p>
                            <strong>Operation Executive:</strong>
                            <br /><span class="text-muted"> <%_out(visit.operationExec);%></span>
                        </p>
                    </div>
                 </div>
                 <div  class="col-md-3">
                    <div class="input-group">
                        <p>
                            <strong>Date:</strong> 
                            <br /><span class="text-muted"> <%_out(visit.date);%></span>
                        </p>
                    </div>
                 </div>
                <div  class="col-md-3">
                    <div class="input-group">
                        <p>
                            <strong>Time:</strong> 
                            <br /><span class="text-muted"> <%_out(visit.time);%></span>
                        </p>
                    </div>
                 </div>
                <div  class="col-md-3">
                    <div class="input-group">
                        <p>
                            <strong>Site:</strong> 
                            <br /><span class="text-muted"> <%_out(visit.site);%></span>
                        </p>
                    </div>
                </div>  
            </div>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>General Car Park Surrounding </h4> 
            <div class="panel-body">
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Car-in-Park:</strong>
                            <p class="text-muted"><%_out(visit.carInPark);%></p>

                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Intercom:</strong>
                            <p class="text-muted"><%_out(visit.intercom);%></p>
                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Spare barrier Arm:</strong>
                            <p class="text-muted"><%_out(visit.spareBarrierArm);%></p>
                        </div>
                 </div>
            </div>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Season Application</h4> 
            <div class="panel-body">
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>COP Book Number and last used page:</strong>
                            <p class="text-muted"><%_out(visit.copBookNo);%></p>
                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Season Terminations:</strong>
                            <p class="text-muted"><%_out(visit.seasonTerminations);%></p>
                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>New Season Applications:</strong>
                            <p class="text-muted"><%_out(visit.newSeasonTerminations);%></p>
                        </div>
                 </div>
            </div>
             <div class="panel-body">
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Refund Forms:</strong>
                            <p class="text-muted"><%_out(visit.refundForms);%></p>
                        </div>
                 </div>
            </div>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Surveillance Camera </h4> 
            <div class="panel-body">
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>CCTV Camera:</strong>
                            <p class="text-muted"><%_out(visit.cctvCamera);%></p>
                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Recording Status:</strong>
                            <p class="text-muted"><%_out(visit.recordingStatus);%></p>
                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Time Syn with PMS:</strong>
                            <p class="text-muted"><%_out(visit.timeSynPms);%></p>
                        </div>
                 </div>
            </div>
             <div class="panel-body">
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Earliest Date of Footage:</strong>
                            <p class="text-muted"><%_out(visit.earliestDateOfFootage);%></p>
                        </div>
                 </div>
            </div>
        </div>
         <div class="bs-callout bs-callout-info"> 
            <h4>Any Other </h4> 
            <div class="panel-body">
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Total Barrier Raise :</strong>
                        <p class="text-muted"><%_out(visit.totalBarrierRaise);%></p>
                    </div>
                </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Receipt Checks (Exit and Top-up Terminal):</strong>
                        <p class="text-muted"><%_out(visit.receiptChecks);%></p>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Containers on Location during visit / details of owner and any discrepancy found:</strong>
                        <p class="text-muted"><%_out(visit.containersOnLoc);%></p>
                    </div>
                </div>
            </div>
             <div class="panel-body">
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Abandoned Vehicle:</strong>
                        <p class="text-muted"><%_out(visit.abandVehicle);%></p>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Surrounding changes that may impact revenue:</strong>
                        <p class="text-muted"><%_out(visit.surroundingChanges);%></p>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Any Other Input and Recommendation for improvements on revenue and general look:</strong>
                        <p class="text-muted"><%_out(visit.remarks);%></p>
                    </div>
                 </div>
            </div>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Image Attachment</h4> 

             <div class="panel-body">
                 <div  class="col-md-4">
                    <div class="input-group">
                        <a onclick="javascript:showModal(1)"><asp:Image ID="image1" class="infoImgThumb img-rounded img-fluid"  runat="server" Width="70%" Height="60%" Visible="false"/></a>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <a onclick="javascript:showModal(2)"><asp:Image ID="image2" class="img-rounded infoImgThumb img-fluid"  runat="server" Width="70%" Height="60%" Visible="false"/></a>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <a onclick="javascript:showModal(3)"><asp:Image ID="image3" class="img-rounded infoImgThumb img-fluid"  runat="server" Width="70%" Height="60%" Visible="false"/></a>
                    </div>
                 </div>
            </div>
        </div>
        <p class="pull-right small">Last Updated by: <%_out(visit.updatedby);%> | Last Updated on: <%_out(visit.lastupdate);%></p>
    </div>

    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Image Attachment Preview</h4>
                </div>
                <div class="modal-body">
                    <img src="" id="imagepreview" style="width: 100%; height: 100%" >
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


</asp:Content>