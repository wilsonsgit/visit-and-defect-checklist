﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarParkCheckList.Utility
{
    public class Constant
    {
 
        public String VISIT_IMG_LOC()
        {
            return "~/UploadedImage/visit/";
        }
        public String DEFECT_IMG_LOC()
        {
            return "~/UploadedImage/defect/";
        }
        public String successMessage(String msg) { 
            return "<div class='alert alert-success'><strong>Success!</strong> "+msg+"</div>";
        }

        public String infoMessage(String msg)
        {
            return "<div class='alert alert-info'><strong>Info!</strong> " + msg+ "</div>";
        }

        public String warningMessage(String msg)
        {
            return "<div class='alert alert-warning'><strong>Warning!</strong> " + msg + "</div>";
        }

        public String dangerMessage(String msg)
        {
            return "<div class='alert alert-danger'><strong>Danger!</strong> " + msg + "</div>";
        }

    }    
}