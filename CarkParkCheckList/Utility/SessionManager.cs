﻿using CarkParkCheckList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarkParkCheckList.Utility
{
    public class SessionManager
    {
        public bool isLogIn()
        {
            return System.Web.HttpContext.Current.Session["isLogin"] != null;
        }

        public void loginUser(User user){
            System.Web.HttpContext.Current.Session["isLogin"] = true;
            System.Web.HttpContext.Current.Session["CURRENT_USER_OBJ"] = user;
            System.Web.HttpContext.Current.Session["CURRENT_USER"] = user.username;
            System.Web.HttpContext.Current.Session["ENC_KEY"] = user.username + DateTime.Now.ToString();

        }

        public void logout()
        {
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.Abandon();

        }
    }
}