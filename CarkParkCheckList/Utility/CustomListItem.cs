﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace CarkParkCheckList.Utility
{
    
    public class CustomListItem
    {

        private Dictionary<String, String> stringDictionary;

        public CustomListItem()
        {
            stringDictionary = new Dictionary<String, String>();
        }

        public Dictionary<String, String> getIntercomSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is working properly.");
            stringDictionary.Add("N", "It is defective and needs replacement.");
            return stringDictionary;
        }
        public Dictionary<String, String> getEqpCleanlSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is clean and tidy.");
            stringDictionary.Add("N", "It is dirty and messy.");
            stringDictionary.Add("N1", "It needs new painting.");
            return stringDictionary;
        }
        public Dictionary<String, String> getRecStatSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is recording properly.");
            stringDictionary.Add("N", "It is defective and not working.");
            stringDictionary.Add("N1", "It's drive is defective.");
            return stringDictionary;
        }
        public Dictionary<String, String> getTimeSyncPMSSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is properly synchronized to PMS.");
            stringDictionary.Add("N", "It is not sychronize to the PMS.");
            stringDictionary.Add("N1", "It is adjusted.");
            return stringDictionary;
        }
        public Dictionary<String, String> getReceiptChecksSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is still sufficient.");
            stringDictionary.Add("N", "It needs to be replenish.");
            return stringDictionary;
        }
        public Dictionary<String, String> getSignSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is clear and visible");
            stringDictionary.Add("N", "It is damaged.");
            stringDictionary.Add("N1", "The paint is faded.");
            stringDictionary.Add("N2", "It is missing.");
            return stringDictionary;
        }
        public Dictionary<String, String> getLightBoxSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is well lighted.");
            stringDictionary.Add("N", "No light at all.");
            return stringDictionary;
        }
        public Dictionary<String, String> getHardwareSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is working properly.");
            stringDictionary.Add("N", "Needs repair/replacement.");
            return stringDictionary;
        }
        public Dictionary<String, String> getBoothSelection()
        {
            stringDictionary = new Dictionary<String, String>();
            stringDictionary.Add("Y", "It is in good condition.");
            stringDictionary.Add("N", "It needs repair/replacement.");
            return stringDictionary;
        }
       
    }


   
    
}