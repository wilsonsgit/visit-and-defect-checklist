﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkCheckList.Models;
using CarParkCheckList.Controllers;
using System.Data.SqlClient;
using System.Globalization;
using CarParkCheckList.Utility;
using System.IO;
using CarkParkCheckList.Utility;

namespace CarkParkCheckList.DefectsForm
{
    public partial class _Default : System.Web.UI.Page
    {
        private static String IMG_PATH = new Constant().DEFECT_IMG_LOC();
        public Defects defects;
        private AES aes;
        private CarParkController carParkController;
        protected String editLink;
        protected String copyLink;
        protected String deleteLink;

        protected void Page_Load(object sender, EventArgs e)
        {
            defects = new Defects();
            aes = new AES();
            carParkController = new CarParkController();
            if (Request["q"] != null && !String.IsNullOrEmpty(Request["q"]))
            {
                string qid = aes.decrypt(Server.HtmlDecode(Request["q"]));
                string id = Request["q"];
                string[] mode = { Server.UrlEncode(aes.encrypt("MODE_1")), Server.UrlEncode(aes.encrypt("MODE_2")) };
                editLink = "./form.aspx?m=" + mode[0] + "&q=" + id;
                copyLink = "./form.aspx?m=" + mode[1] + "&q=" + id;
                deleteLink = "onclick=deleteView('" + id + "')";
                populateData(qid);
            }
        }

        private void populateData(String id)
        {
            CustomListItem cli = new CustomListItem();
            defects = carParkController.getDefectDataByID(id);

            if (Session["CURRENT_USER"].Equals(defects.operationExec))
                infoAction.Visible = true;
            carParkController.closeSQLConn();

            defects.equipmentCleanliness = cli.getEqpCleanlSelection()[defects.equipmentCleanliness];
            defects.lightBox = cli.getLightBoxSelection()[defects.lightBox];
            defects.scanningIssues = cli.getHardwareSelection()[defects.scanningIssues];
            defects.cardReader = cli.getHardwareSelection()[defects.cardReader];
            defects.acpipe = cli.getBoothSelection()[defects.acpipe];
            defects.boothFlooring = cli.getBoothSelection()[defects.boothFlooring];
            defects.boothCondition = cli.getBoothSelection()[defects.boothCondition];
            defects.boothWrap = cli.getBoothSelection()[defects.boothWrap];
            getImages(id);
        }

        private void getImages(String id)
        {
            String path = IMG_PATH + id + "/";
            Image[] img = { image1, image2, image3 };
            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
            if (exists)
            {
                string[] fileEntries = Directory.GetFiles(Server.MapPath(path));

                for (int i = 0; i < fileEntries.Length; i++)
                {

                    String filenameRelative = path + Path.GetFileName(fileEntries[i]);
                    img[i].Visible = true;
                    img[i].ImageUrl = ResolveClientUrl(filenameRelative);

                }
            }
        }

        public void _out(String str)
        {
            Response.Write(str);
        }
    }
}