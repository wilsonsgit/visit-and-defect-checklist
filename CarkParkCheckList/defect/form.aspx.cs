﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkCheckList.Models;
using CarParkCheckList.Controllers;
using CarParkCheckList.Utility;
using CarkParkCheckList.Utility;
using System.Globalization;
using System.Data;
using System.Web.UI.HtmlControls;
using System.IO;


namespace CarParkCheckList.DefectsForm
{
    public partial class _Default : System.Web.UI.Page
    {
        private static String IMG_PATH = new Constant().DEFECT_IMG_LOC();
        private DateTime dtDate;
        public String formId;
        public String mode;
        private AES aes;
        private CarParkController carParkController;
        private Defects currentDefectData;

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            aes = new AES();
            carParkController = new CarParkController();
            formId = " (New Application)";
            if (Request.QueryString["q"] != null && Request.QueryString["m"] != null)
            {
                try
                {
                    formId = aes.decrypt(Server.HtmlDecode(Request["q"]));
                    mode = aes.decrypt(Server.HtmlDecode(Request["m"]));

                    if (mode == "MODE_1")
                    {
                        Session["INFO_MSG"] = new Constant().dangerMessage("Form is now open for changes.");
                        Session["INFO_LOC_MSG"] = "Edit Form";
                    }
                    else
                    {
                        Session["INFO_MSG"] = new Constant().infoMessage("Form has been copied.");
                        Session["INFO_LOC_MSG"] = "Copy Form";

                    }

                    currentDefectData = carParkController.getDefectDataByID(formId);
                    if (!IsPostBack)
                    {
                        initSelector();
                        initFieldsContent(currentDefectData);
                        if (mode == "MODE_2")
                            initSpecial();
                    }

                }
                catch
                {
                    Response.Redirect("./");
                }
            }
            else
            {
                if (!IsPostBack)
                {
                    initSpecial();
                    initSelector();
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "Car Park Defects Form";
          
        }

        private void initSpecial()
        {
            txtDate.Text = System.DateTime.Now.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            txtTime.Text = System.DateTime.Now.ToString("HH:mm", CultureInfo.InvariantCulture);
            txtOperationExacutive.Text = (string)Session["CURRENT_USER"];
            txtOperationExacutive.Text = (string)Session["CURRENT_USER"];
        }

        private void initSelector()
        {
            CustomListItem listItem = new CustomListItem();
            DataSet ds = new CarParkController().getCpcodeDataSet();
            txtSite.DataSource = ds;
            txtSite.DataTextField = "CPCODE";
            txtSite.DataValueField = "CPCODE";
            txtSite.DataBind();

            dataBind(txtEquipmentCleanliness, listItem.getEqpCleanlSelection());
            dataBind(txtLightBox, listItem.getLightBoxSelection());
            dataBind(txtCardReader, listItem.getHardwareSelection());
            dataBind(txtScanningIssues, listItem.getHardwareSelection());
            dataBind(txtAirconDrainaigePipe, listItem.getBoothSelection());
            dataBind(txtBoothCondition, listItem.getBoothSelection());
            dataBind(txtBoothFlooring, listItem.getBoothSelection());
            dataBind(txtBoothWrap, listItem.getBoothSelection());
        }


        private void dataBind(DropDownList dl, Dictionary<String, String> dic)
        {
            dl.DataSource = dic;
            dl.DataTextField = "Value";
            dl.DataValueField = "Key";
            dl.DataBind();
        }

        private void initFieldsContent(Defects defects)
        {
            dtDate = DateTime.ParseExact(defects.date, "dd MMM yyyy", CultureInfo.InvariantCulture);
            txtDate.Text = dtDate.ToString("MM/dd/yyyy");
            dtDate = DateTime.Parse(defects.time, CultureInfo.InvariantCulture);
            txtTime.Text = dtDate.ToString("HH:mm");

            txtOperationExacutive.Text = defects.operationExec;
            txtSite.SelectedValue = defects.site;
            txtBollards.Value = defects.bollards;
            txtEquipmentCleanliness.SelectedValue = defects.equipmentCleanliness;
            txtSign.Value = defects.sign;
            txtSpotLight.Value = defects.spotLight;
            txtLightBox.SelectedValue = defects.lightBox;
            txtScrapBarrier.Value = defects.scrapbarrier;
            txtSafetyConcerns.Value = defects.safetyConcerns;
            txtCarParkLights.Text = defects.carParkLights;
            txtPotHoles.Value = defects.potHoles;
            txtGeneralCleanliness.Value = defects.generalCleanliness;
            txtWaterPounding.Value = defects.generalCleanliness;
            txtFadedPaintWorks.Value = defects.fadedPaintWorks;
            txtOilLeak.Value = defects.oilLeak;
            txtWaterPounding.Value = defects.waterPounding;
            txtWaterPoundingCP.Value = defects.waterPoundingcpl;
            txtPGSSystem.Value = defects.pgsSystem;
            txtDamageSurface.Value = defects.dmgSurface;
            txtScanningIssues.SelectedValue = defects.scanningIssues;
            txtCardReader.SelectedValue = defects.cardReader;
            txtCCTV.Value = defects.cctv;
            txtSystemKiosk.Value = defects.systemKiosk;
            txtBarrierPaddingArm.Value = defects.barrierPadding;
            txtCableTie.Value = defects.cableTie;
            txtAirconDrainaigePipe.SelectedValue = defects.acpipe;
            txtBoothCondition.SelectedValue = defects.boothCondition;
            txtBoothFlooring.SelectedValue = defects.boothFlooring;
            txtBoothWrap.SelectedValue = defects.boothWrap;
            txtBarrierKiosk.Value = defects.barrierKiosk;
            txtRemarks.Value = defects.remarks;

            getImages(defects.jobid);
        }

        private void getImages(String id)
        {
            String path = IMG_PATH + id + "/";
            HtmlInputFile[] imgInput = { imageInput1, imageInput2, imageInput3 };
            HtmlGenericControl[] imgView = { imageInput1_view, imageInput2_view, imageInput3_view };
            Image[] imgtag = { image1, image2, image3 };

            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
            if (exists)
            {

                string[] fileEntries = getImagesLocation(id);

                for (int i = 0; i < fileEntries.Length; i++)
                {
                    imgView[i].Visible = true;
                    String filenameRelative = path + Path.GetFileName(fileEntries[i]);
                    imgtag[i].ImageUrl = ResolveClientUrl(filenameRelative);
                }
            }
        }

        private string[] getImagesLocation(String id)
        {
            string[] fileEntries = null;
            String path = IMG_PATH + id + "/";
            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
            if (exists)
                fileEntries = Directory.GetFiles(Server.MapPath(path));

            return fileEntries;
        }

        protected void btnListener(object sender, EventArgs e)
        {
            //MODE_1 - EDIT; MODE2 - COPY/NEW
            Defects defects = new Defects();
            bool IS_UPDATE = false;
            Session["INFO_MSG"] = new Constant().successMessage("Data has been saved.");
            if (mode == "MODE_1")
            {
                IS_UPDATE = true;
                Session["INFO_MSG"] = new Constant().successMessage("Data has been updated.");
            }

           try
            {
                Object insertedDataId = null;
                defects = getFormData(IS_UPDATE);
                insertedDataId = new CarParkController().saveCarparkDefects(defects, IS_UPDATE);
                if (insertedDataId != null)
                {
                    createdImgFolder(insertedDataId);
                }
                if (IS_UPDATE)
                {
                    updateImage(defects.jobid);
                }

            }
            catch (Exception)
            {
                Session["INFO_MSG"] = new Constant().warningMessage("An error occur while saving your form, please check your data/internet connection.");
            }
            Response.Redirect("../home.aspx");
        }

        private void updateImage(String id)
        {
            String[] imagesLoc = getImagesLocation(id);
            HtmlInputFile[] imgInput = { imageInput1, imageInput2, imageInput3 };
            HiddenField[] dlist = { d1, d2, d3 };


            for (int i = 0; i < dlist.Length; i++)
            {
                if (dlist[i].Value.Equals("1"))
                {
                    File.Delete(ResolveClientUrl(imagesLoc[i]));
                }
            }
            createdImgFolder(id);
        }

        private void createdImgFolder(Object fname)
        {
            String path = IMG_PATH + fname;
            bool exists = System.IO.Directory.Exists(Server.MapPath(path));

            if (!exists)
                System.IO.Directory.CreateDirectory(Server.MapPath(@path));

            uploadImage(fname, imageInput1);
            uploadImage(fname, imageInput2);
            uploadImage(fname, imageInput3);
        }

        private void uploadImage(Object path, HtmlInputFile file)
        {
            var loc = Server.MapPath(@IMG_PATH + path + "/" + file.PostedFile.FileName);
            try
            {
                file.PostedFile.SaveAs(loc);
            }
            catch (Exception) { }
        }

        private Defects getFormData(bool isUpdate)
        {
            Defects defects = new Defects();
            if (!isUpdate)
            {
                dtDate = DateTime.ParseExact(txtDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                defects.date = dtDate.ToString("yyyy-MM-dd");
                defects.operationExec = txtOperationExacutive.Text;
                defects.time = txtTime.Text;
                defects.createdby = (String)Session["CURRENT_USER"];
            }
            else
            {
                defects.jobid = currentDefectData.jobid;
            }

            defects.site = txtSite.SelectedValue;
            defects.bollards = txtBollards.Value;
            defects.equipmentCleanliness = txtEquipmentCleanliness.SelectedValue;
            defects.sign = txtSign.Value;
            defects.spotLight = txtSpotLight.Value;
            defects.lightBox = txtLightBox.SelectedValue;
            defects.scrapbarrier = txtScrapBarrier.Value;
            defects.safetyConcerns = txtSafetyConcerns.Value;
            defects.carParkLights = txtCarParkLights.Text;
            defects.potHoles = txtPotHoles.Value;
            defects.generalCleanliness = txtGeneralCleanliness.Value;
            defects.fadedPaintWorks = txtFadedPaintWorks.Value;
            defects.oilLeak = txtOilLeak.Value;
            defects.waterPounding = txtWaterPounding.Value;
            defects.waterPoundingcpl = txtWaterPoundingCP.Value;
            defects.pgsSystem = txtPGSSystem.Value;
            defects.dmgSurface = txtDamageSurface.Value;
            defects.scanningIssues = txtScanningIssues.SelectedValue;
            defects.cardReader = txtCardReader.SelectedValue;
            defects.cctv = txtCCTV.Value;
            defects.systemKiosk = txtSystemKiosk.Value;
            defects.barrierPadding = txtBarrierPaddingArm.Value;
            defects.cableTie = txtCableTie.Value;
            defects.acpipe = txtAirconDrainaigePipe.SelectedValue;
            defects.boothCondition = txtBoothCondition.SelectedValue;
            defects.boothFlooring = txtBoothFlooring.SelectedValue;
            defects.boothWrap = txtBoothWrap.SelectedValue;
            defects.barrierKiosk = txtBarrierKiosk.Value;
            defects.remarks = txtRemarks.Value;
            defects.updatedby = (String)Session["CURRENT_USER"];
            defects.lastupdate = System.DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            return defects;
        } 
    }
}