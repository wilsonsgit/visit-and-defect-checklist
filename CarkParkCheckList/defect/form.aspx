﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="form.aspx.cs" Inherits="CarParkCheckList.DefectsForm._Default" MasterPageFile="~/CarParkCheckList.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="bodyTitle" runat="server">
    <h3 class="page-header">
        <span class="text-muted">Check List <i class="fa fa-angle-double-right fa-fw"></i></span>
        <a href="./">Defect</a>
        <span class="fa fa-angle-double-right fa-fw text-muted"></span>
        <span class="text-primary"><%Response.Write(Session["INFO_LOC_MSG"]);%></span>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="messageContainer" runat="server">
    <%
        if (Session["INFO_MSG"] != null)
        {
            Response.Write(Session["INFO_MSG"]);
            Session["INFO_MSG"] = null;
        }
          
     %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server">
    <form runat="server">
    <div class="form-group">
        <div class="panel-group" id="accordion">
            <div  class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Car Park Defects Form
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div  class="col-md-3">
                            <label for="email">Operation Executive</label>
                            <asp:TextBox ID="txtOperationExacutive" type="text" class="form-control txtOperationExacutive"  runat="server"></asp:TextBox>
                        </div>
                        <div  class="col-md-3">
                            <label for="pwd">Site</label>
                             <asp:DropDownList runat="server"  ID="txtSite" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                        </div>
                        <div  class="col-md-3">
                            <label class="control-label" for="date">Date</label>
                            <div class="input-group">
                                <asp:TextBox ID="txtDate" type="text" class="form-control txtDate"  runat="server" ReadOnly></asp:TextBox>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>


                        <div  class="col-md-3">
                                <label class="control-label" for="time">Time</label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <asp:TextBox ID="txtTime" type="text" class="form-control txtTime"  runat="server" ReadOnly>
                                    </asp:TextBox>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" href="#collapse1">
                            <a>
                                <span class="pull-right"><span id="Glyp-Col1" class="glyphicon glyphicon-chevron-down"></span></span>
                                General Car Park Surrounding
                            </a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                             <div class="col-md-4">
                                <asp:Label ID="Label2" class="control-label" runat="server" Text="Equipment Cleanliness"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtEquipmentCleanliness" class="demo-default txtSelect" required></asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label5" class="control-label" runat="server" Text="Light-Box"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtLightBox" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label8" class="control-label" runat="server" Text="Car Park Lights"></asp:Label>
                                <asp:TextBox ID="txtCarParkLights" type="number" class="form-control"  runat="server" Text="1" min="1" max="7" step="1" data-bind="value:bodyContainer_txtCarParkLights" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label1" class="control-label" runat="server" Text="Bollards"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtBollards" runat="server" placeholder="Bollards - Remarks"></Textarea>
                            </div>
                             <div class="col-md-4">
                                <asp:Label ID="Label4" class="control-label" runat="server" Text="Spot Light"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtSpotLight" runat="server" placeholder="Location - Fault"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label3" class="control-label" runat="server" Text="Sign"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtSign" runat="server" placeholder="Sign - Remarks"></Textarea>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label6" class="control-label" runat="server" Text="Scrap Barrier"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtScrapBarrier" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label11" class="control-label" runat="server" Text="Water Pounding"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtWaterPounding" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label9" class="control-label" runat="server" Text="Pot Holes"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtPotHoles" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label7" class="control-label" runat="server" Text="Safety Concerns"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtSafetyConcerns" runat="server"></Textarea>
                            </div>
                           
                            <div class="col-md-4">
                                <asp:Label ID="Label10" class="control-label" runat="server" Text="General Cleanliness"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtGeneralCleanliness" runat="server"></Textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" href="#collapse2">
                            <a>
                                <span class="pull-right"><span id="Glyp-Col2" class="glyphicon glyphicon-chevron-down"></span></span>
                                    Car Park Lots
                            </a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label13" class="control-label" runat="server" Text="Faded Paint Works"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtFadedPaintWorks" runat="server" placeholder="Lot # - Remarks"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label14" class="control-label" runat="server" Text="Oil Leak"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtOilLeak" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label15" class="control-label" runat="server" Text="Water Pounding"></asp:Label>
                                 <Textarea class="form-control" rows="5" ID="txtWaterPoundingCP" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label16" class="control-label" runat="server" Text="PGS System"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtPGSSystem" runat="server" placeholder="Lot # - Remarks"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label17" class="control-label" runat="server" Text="Damage Surface"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtDamageSurface" runat="server" placeholder="Lot # - Remarks"></Textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" href="#collapse3">
                            <a>
                                <span class="pull-right"><span id="Glyp-Col3" class="glyphicon glyphicon-chevron-down"></span></span>
                                Hardware
                            </a>
                            </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label18" class="control-label" runat="server" Text="Scanning Issues"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtScanningIssues" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label19" class="control-label" runat="server" Text="Card Reader"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtCardReader" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label20" class="control-label" runat="server" Text="CCTV"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtCCTV" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label21" class="control-label" runat="server" Text="System Kiosk"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtSystemKiosk" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                           
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label23" class="control-label" runat="server" Text="Barrier Padding / Barrier Arm "></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtBarrierPaddingArm" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label22" class="control-label" runat="server" Text="Barrier Kiosk"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtBarrierKiosk" runat="server" placeholder="Location - Remarks"></Textarea>

                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label24" class="control-label" runat="server" Text="Cable Tie"></asp:Label>
                                <Textarea class="form-control" rows="5" ID="txtCableTie" runat="server" placeholder="Location - Remarks"></Textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"  data-toggle="collapse" href="#collapse4">
                            <a>
                                <span class="pull-right"><span id="Glyp-Col4" class="glyphicon glyphicon-chevron-down"></span></span>
                                Car Park Booth
                            </a>
                            </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label25" class="control-label" runat="server" Text="Aircon & Drainage Pipe"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtAirconDrainaigePipe" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>

                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label26" class="control-label" runat="server" Text="Booth Condition"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtBoothCondition" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <asp:Label ID="Label27" class="control-label" runat="server" Text="Booth Flooring"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtBoothFlooring" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label28" class="control-label" runat="server" Text="Booth Wrap"></asp:Label>
                                <asp:DropDownList runat="server"  ID="txtBoothWrap" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" href="#collapse5">
                            <a>
                                <span class="pull-right"><span id="Glyp-Col5" class="glyphicon glyphicon-chevron-down"></span></span>
                            Any Other Deffects</a>
                            </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <asp:Label ID="Label29" class="control-label" runat="server" Text="Remarks"></asp:Label>
                                <textarea class="form-control" rows="5" ID="txtRemarks" runat="server"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse6">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col5" class="glyphicon glyphicon-chevron-down"></span></span>
                                    Image Attachment
                                </a>
                             </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <input ID="imageInput1"  type="file" class="file file-loading imageInput" data-allowed-file-extensions='["png", "jpg", "jpeg"]' runat="server"/>
                                    <asp:HiddenField ID="d1" runat="server" value="0"/>
                                    <div class="input-group imageInput1_view"  runat="server" ID="imageInput1_view" visible="false">
                                        <asp:Image ID="image1" class="infoImgThumb img-rounded"  runat="server" Width="304" Height="236" ImageUrl="../UploadedImage/54/Jellyfish.jpg"/>
                                        <a href='javascript:deleteImg(1)' class="btn btn-danger">DELETE</a>
                                    </div>
                               </div>
                               <div class="col-md-4">
                                    <input ID="imageInput2" type="file" class="file file-loading imageInput" data-allowed-file-extensions='["png", "jpg", "jpeg"]' runat="server"/>
                                    <asp:HiddenField ID="d2" runat="server" Value="0" />
                                    <div class="input-group imageInput2_view"  runat="server" ID="imageInput2_view" visible="false">
                                        <asp:Image ID="image2" class="infoImgThumb img-rounded"  runat="server" Width="304" Height="236" ImageUrl="../UploadedImage/54/Jellyfish.jpg"/>
                                        <a href='javascript:deleteImg(2)' class="btn btn-danger">DELETE</a>
                                    </div>
                               </div>
                               <div class="col-md-4">
                                    <input ID="imageInput3" type="file" class="file file-loading imageInput" data-allowed-file-extensions='["png", "jpg", "jpeg"]' runat="server"/>
                                    <asp:HiddenField ID="d3" runat="server" Value="0" />
                                    <div class="input-group imageInput3_view" runat="server" ID="imageInput3_view" visible="false">
                                        <asp:Image ID="image3" class="infoImgThumb img-rounded"  runat="server" Width="304" Height="236" ImageUrl="../UploadedImage/54/Jellyfish.jpg"/>
                                        <a href='javascript:deleteImg(3)' class="btn btn-danger">DELETE</a>
                                    </div>
                               </div>
                            </div>
                             
                        </div>
                    </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <asp:Button ID="Button1" class="btn btn-primary btn-lg btn-block" runat="server" Text="Save" OnClick="btnListener"/>
                    </div>     
                </div>
            </div>
        </div>
    </div> 
    </form>
</asp:Content>

