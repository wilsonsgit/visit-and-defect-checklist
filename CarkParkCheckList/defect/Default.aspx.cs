﻿using CarParkCheckList.Controllers;
using CarParkCheckList.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarkParkCheckList.Defect
{
    public partial class _Default : System.Web.UI.Page
    {
        private CarParkController carParkController;

        private static String IMG_PATH = new Constant().DEFECT_IMG_LOC();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["d"] != null && !String.IsNullOrEmpty(Request["d"]))
            {
                AES aes = new AES();
                deleteFile(aes.decrypt(Server.HtmlDecode(Request["d"])));
            }
        }

        private void deleteFileImage(String id)
        {
            String path = IMG_PATH + id + "/";

            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
            if (exists)
                Directory.Delete(Server.MapPath(path), true);

        }

        private void deleteFile(String id)
        {
            new CarParkController().deleteDefectData(id);
            deleteFileImage(id);
            Response.Redirect("./");
        }

        public String renderTableView()
        {
            AES aes = new AES();
            SqlDataReader dReader = getDefectData();
            String str = "";
            while (dReader.Read())
            {
                String edt = "&nbsp;";
                String copy = "&nbsp;";
                String delete = "&nbsp;";
                string qid = Server.UrlEncode(aes.encrypt(dReader.GetValue(0).ToString()));
                string[] mode = { Server.UrlEncode(aes.encrypt("MODE_1")), Server.UrlEncode(aes.encrypt("MODE_2")) };
               
                if (String.Compare((String)Session["CURRENT_USER"], (String)dReader.GetValue(1), true) == 0)
                {
                    edt = "<a href='./form.aspx?m=" + mode[0] + "&q=" + qid + "' title='Edit Row'><span class='glyphicon glyphicon-edit btn btn-small'></span></a>";
                    copy = "<a href='./form.aspx?m=" + mode[1] + "&q=" + qid + "' title='Copy Row'><span class='glyphicon glyphicon-copy btn btn-small'></span></a>";
                    delete = "<a href='#' onclick=deleteView('" + qid + "') title='Delete Row'><span class='glyphicon glyphicon-remove btn btn-small'></span></a>";
                }

                String view = "<a href='./info.aspx?q=" + qid + "' title='View Details'><span class='glyphicon glyphicon-eye-open btn btn-small'></span></a>";
                DateTime dtTime = DateTime.Parse(dReader["TIME"].ToString(), CultureInfo.InvariantCulture);
                str += "<tr>" +
                    "<td >" +
                        "<div class='btn-group inline'>" + view + edt + copy + delete + "</div>" +
                    "</td>" +
                    "<td>" + dReader.GetValue(1) + "</td>" +
                    "<td>" + dReader.GetValue(2) + "</td>" +
                    "<td>" + dReader.GetDateTime(3).ToString("dd MMM yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "</td>" +
                    "<td>" + dtTime.ToString("hh:mm tt") + "</td>" +
                    "</tr>";
            }
            closeConn();
            return str;
        }

        private SqlDataReader getDefectData()
        {
            carParkController = new CarParkController();
            return carParkController.getDefectData();
        }

        private void closeConn()
        {
            carParkController.closeSQLConn();
        }
    }
}