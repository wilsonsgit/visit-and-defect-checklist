﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarParkCheckList.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="CarPark.Home" %>
                            


<asp:Content ID="Content3" ContentPlaceHolderID="bodyTitle" runat="server">
     <h3 class="page-header"><span class="text-primary">Dashboard</span></h3>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="messageContainer" runat="server">
    <%
        if (Session["INFO_MSG"] != null)
        {
            Response.Write(Session["INFO_MSG"]);
            Session["INFO_MSG"] = null;
        }
     %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server">
    <div class="container-fluid col-md-12">
        <div class="row">
            <%getTiles("paperclip",getTotVisit(),"#","Total Visit Today", "primary");%>
            <%getTiles("wrench", getTotDefect(), "#", "Total Defect Today", "green");%> 
            <%getTiles("home", getTotDefect() + getTotVisit(), "#", "Total for Today", "yellow");%> 
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Checklist Submitted
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="?stat=0">7 days</a></li>
                                    <li><a href="?stat=1">7 Months</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Statistician</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Activity Log
                        </div>
                        <div class="panel-body">
                            <div class="list-group">
                                <%getLogs();%>
                             </div>
                        </div>
                    </div>
            </div>
        </div>       
    </div>
    <script type="text/javascript">
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: '7 Days - Forms Stat'
                },
                subtitle: {
                    text: 'Source: IT - Wilson Parking (SG)'
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'TOTAL'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                }
                <%plotGraphData();%>

            });
        });
    </script>
</asp:Content>
