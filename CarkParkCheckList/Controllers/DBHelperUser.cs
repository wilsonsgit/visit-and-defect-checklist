﻿using CarParkCheckList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Web.Http;
using System.Configuration;
using System.Globalization;
using CarkParkCheckList.Models;

namespace CarParkCheckList.Controllers
{
    public class DBHelperUser : ApiController
    {
        private static String sConString = ConfigurationManager.ConnectionStrings["CarparkCentralConnectionString"].ToString();
        private SqlConnection sqlConnection;



        public List<User> getUserList(string sitecode)
        {
            string sSQL =
                "SELECT   A.ID AS USERID, A.USERNAME, A.IS_LOCKED, CONVERT(varchar, B.REQUESTTIMESTAMP, 100) AS LASTLOGIN " +
                "FROM [Users].[dbo].[SITE_ALLOWED_USERS] A "+
                "LEFT JOIN ( "+
	                "SELECT REQUESTBY,REQUESTTIMESTAMP,ROW_NUMBER() OVER (PARTITION BY REQUESTBY ORDER BY REQUESTTIMESTAMP DESC) AS ROW_NUM "+
	                "FROM [Users].[dbo].[USER_SESSION] "+
                ")AS B "+
                "ON A.USERNAME = B.REQUESTBY "+
                "WHERE ROW_NUM = 1 OR ROW_NUM IS NULL "+
                "AND A.SITE_ID = '"+ sitecode + "' "+
                "ORDER BY  A.USERNAME ";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();

            User user = null;
            List<User> userList = new List<User>();
            if(reader.HasRows){
                while(reader.Read()){
                    user = new User();
                    user.userid = reader["USERID"].ToString();
                    user.username = reader["USERNAME"].ToString();
                    user.islock = String.Compare(reader["IS_LOCKED"].ToString(),"Y", true) == 0 ? "Locked" : "Active";
                    user.lastdatetimelogin = reader["LASTLOGIN"].ToString(); 
                    userList.Add(user);
                }
            }
            closeSQLConn();
            return userList;
        }

        public DataSet getUserGroup(string sid)
        {

            string sSQL = "SELECT [ROLEID],[DESCRIPTION] FROM [Users].[dbo].[USER_ROLE] WHERE SITEID = '" + sid + "'";
            sqlConnection = new SqlConnection(sConString);
            SqlDataAdapter cmd = new SqlDataAdapter(sSQL, sqlConnection);
            DataSet ds = new DataSet();
            cmd.Fill(ds, "SITES_PERMISSION");
            return ds;
        }

        public void closeSQLConn()
        {
            sqlConnection.Close();
        }
    }
}
