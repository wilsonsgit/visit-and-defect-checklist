﻿using CarParkCheckList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Web.Http;
using System.Configuration;
using System.Globalization;
using CarkParkCheckList.Models;

namespace CarParkCheckList.Controllers
{
    public class CarParkController : ApiController
    {
        private static String sConString = ConfigurationManager.ConnectionStrings["CarparkCentralConnectionString"].ToString();
        private SqlConnection sqlConnection;

        [HttpPost]
        public Object saveCarparkDefects([FromBody]Defects defect, bool isUpdate)
        {
            using (SqlConnection con = new SqlConnection(sConString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    string sSQL = "";

                    if (!isUpdate)
                        sSQL = "INSERT INTO [CarParkCentral].[dbo].[CarParkJobDefects]" +
                                    " VALUES(@cpcode,@bollards,@eqpclean,@signs,@spotlight,@lightbox,@scrapbarr,@safetyconc,@carparklghts,@potholes,@gencleanlnss,@waterpounding,@fadedpaint,@oilleaks,@waterpoundingcpl,@pgssystem,@dmgsurface,@scanningissues,@cardreader,@cctvsystem,@systemkiosk,@barrierkiosk,@barrierpaddingarm,@cabletie,@acdrainagepipe,@boothcondition,@boothflooring,@boothwrap,@otherdefects,'" + defect.date + "','" + defect.date + "',@updatedby,@createdby,@time); SELECT SCOPE_IDENTITY()";
                    else
                        sSQL =  "UPDATE [CarParkCentral].[dbo].[CarParkJobDefects] SET" +
                                    " CPCODE = @cpcode," +
                                    " BOLLARDS = @bollards," +
                                    " EQUIPMENT_CLEANLINESS = @eqpclean," +
                                    " SIGNS = @signs," +
                                    " SPOT_LIGHT = @spotlight," +
                                    " LIGHT_BOX = @lightbox," +
                                    " SCRAP_BARRIER = @scrapbarr," +
                                    " SAFETY_CONCERNS = @safetyconc," +
                                    " CARpARK_LIGHTS = @carparklghts," +
                                    " POT_HOLES = @potholes," +
                                    " GEN_CLEANLINES = @gencleanlnss," +
                                    " WATER_POUNDING = @waterpounding," +
                                    " FADED_PAINTWORKS = @fadedpaint," +
                                    " OIL_LEAK = @oilleaks," +
                                    " WATERPOUNDING_CPL = @waterpoundingcpl," +
                                    " PGS_SYSTEM = @pgssystem," +
                                    " DAMAGE_SURFACE = @dmgsurface," +
                                    " SCANNINGISSUES = @scanningissues," +
                                    " CARD_READER = @cardreader," +
                                    " CCTV = @cctvsystem," +
                                    " SYSTEM_KIOSKS = @systemkiosk," +
                                    " BARRIER_KIOSKS = @barrierkiosk," +
                                    " BARRIER_PADDING_ARM = @barrierpaddingarm," +
                                    " CABLE_TIE = @cabletie," +
                                    " AC_DRAINAGE_PIPE = @acdrainagepipe," +
                                    " BOOTH_CONDITION = @boothcondition," +
                                    " BOOTH_FLOORING = @boothflooring," +
                                    " BOOTH_WRAP = @boothwrap," +
                                    " OTHER_DEFECTS = @otherdefects," +
                                    " LASTUPDATE = '" + defect.lastupdate +"',"+
                                    " UPDATEDBY = @updatedby" +
                                    " WHERE JOBID = @jobid";        
            


                    con.Open();
                    cmd.Connection = con;
                    cmd.CommandText = sSQL;

                    cmd.Parameters.AddWithValue("@cpcode", defect.site);
                    cmd.Parameters.AddWithValue("@bollards", defect.bollards);
                    cmd.Parameters.AddWithValue("@eqpclean", defect.equipmentCleanliness);
                    cmd.Parameters.AddWithValue("@signs", defect.sign);
                    cmd.Parameters.AddWithValue("@spotlight", defect.spotLight);
                    cmd.Parameters.AddWithValue("@lightbox", defect.lightBox);
                    cmd.Parameters.AddWithValue("@scrapbarr", defect.scrapbarrier);
                    cmd.Parameters.AddWithValue("@safetyconc", defect.safetyConcerns);
                    cmd.Parameters.AddWithValue("@carparklghts", defect.carParkLights);
                    cmd.Parameters.AddWithValue("@potholes", defect.potHoles);
                    cmd.Parameters.AddWithValue("@gencleanlnss", defect.generalCleanliness);
                    cmd.Parameters.AddWithValue("@waterpounding", defect.waterPounding);
                    cmd.Parameters.AddWithValue("@fadedpaint", defect.fadedPaintWorks);
                    cmd.Parameters.AddWithValue("@pgssystem", defect.pgsSystem);
                    cmd.Parameters.AddWithValue("@oilleaks", defect.oilLeak);
                    cmd.Parameters.AddWithValue("@waterpoundingcpl", defect.waterPoundingcpl);
                    cmd.Parameters.AddWithValue("@dmgsurface", defect.dmgSurface);
                    cmd.Parameters.AddWithValue("@scanningissues", defect.scanningIssues);
                    cmd.Parameters.AddWithValue("@cardreader", defect.cardReader);
                    cmd.Parameters.AddWithValue("@cctvsystem", defect.cctv);
                    cmd.Parameters.AddWithValue("@systemkiosk", defect.systemKiosk);
                    cmd.Parameters.AddWithValue("@barrierkiosk", defect.barrierKiosk);
                    cmd.Parameters.AddWithValue("@barrierpaddingarm", defect.barrierPadding);
                    cmd.Parameters.AddWithValue("@cabletie", defect.cableTie);
                    cmd.Parameters.AddWithValue("@acdrainagepipe", defect.acpipe);
                    cmd.Parameters.AddWithValue("@boothcondition", defect.boothCondition);
                    cmd.Parameters.AddWithValue("@boothflooring", defect.boothFlooring);
                    cmd.Parameters.AddWithValue("@boothwrap", defect.boothWrap);
                    cmd.Parameters.AddWithValue("@otherdefects", defect.remarks);
                    cmd.Parameters.AddWithValue("@updatedby", defect.updatedby);

                    if (isUpdate)
                    {
                        cmd.Parameters.AddWithValue("@jobid", defect.jobid);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@CREATEDBY", defect.createdby);
                        cmd.Parameters.AddWithValue("@TIME", defect.time);
                    }

                    Object indentityID = cmd.ExecuteScalar();
                    con.Close();
                    return indentityID;
                }
            }
        }

        [HttpPost]
        public Object saveCarparkVisits([FromBody]Visit visit, bool isUpdate)
        {
            using (SqlConnection con = new SqlConnection(sConString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                     string sSQL="";
                     if (!isUpdate)
                     {
                         sSQL = "INSERT INTO [CarParkCentral].[dbo].[CarParkJobVisits]" +
                           "VALUES(@CPCODE,@CARINPARK,@INTERCOM,@SPARE_BARRIER_ARM,@BUILDING_TENACY,@EQPT_CLEANL,@COP,@SEASON_TERM,@NEW_SEASON_APP,@REFUND_FORMS,@CCTV_CAM,@RECORDING_STAT,@TIME_SYN_PMS,@EARLIEST_FOOTAGE,@TOT_BAR_RAISE,@RECEIPT_CHECKS,@CONT_ON_LOC,@ABAND_VEHICLE,@SURRO_CHANGES,@REMARKS,'" + visit.date + "','" + visit.date + "',@UPDATEDBY,@CREATEDBY,@TIME); SELECT SCOPE_IDENTITY()";
                     }
                     else
                     {
                         sSQL = "UPDATE [CarParkCentral].[dbo].[CarParkJobVisits] SET "
                                + " CPCODE =  @CPCODE,"
                                + " CARINPARK =  @CARINPARK,"
                                + " INTERCOM = @INTERCOM,"
                                + " SPARE_BARRIER_ARM = @SPARE_BARRIER_ARM,"
                                + " BUILDING_TENACY = @BUILDING_TENACY,"
                                + " EQPT_CLEANL = @EQPT_CLEANL,"
                                + " COP =  @COP,"
                                + " SEASON_TERM = @SEASON_TERM,"
                                + " NEW_SEASON_APP =  @NEW_SEASON_APP,"
                                + " REFUND_FORMS = @REFUND_FORMS,"
                                + " CCTV_CAM = @CCTV_CAM,"
                                + " RECORDING_STAT = @RECORDING_STAT,"
                                + " TIME_SYN_PMS = @TIME_SYN_PMS,"
                                + " EARLIEST_FOOTAGE = @EARLIEST_FOOTAGE,"
                                + " TOT_BAR_RAISE = @TOT_BAR_RAISE,"
                                + " RECEIPT_CHECKS = @RECEIPT_CHECKS,"
                                + " CONT_ON_LOC = @CONT_ON_LOC,"
                                + " ABAND_VEHICLE = @ABAND_VEHICLE,"
                                + " SURRO_CHANGES = @SURRO_CHANGES,"
                                + " REMARKS = @REMARKS,"
                                + " LASTUPDATE = '" + visit.lastupdate +"',"
                                + " UPDATEDBY = @UPDATEDBY"
                                + " WHERE JOBID= @jobid";
                     }
                    con.Open();
                    cmd.Connection = con;
                    cmd.CommandText = sSQL;

                    
                    cmd.Parameters.AddWithValue("@CPCODE", visit.site);
                    cmd.Parameters.AddWithValue("@CARINPARK", visit.carInPark);
                    cmd.Parameters.AddWithValue("@INTERCOM", visit.intercom);
                    cmd.Parameters.AddWithValue("@SPARE_BARRIER_ARM", visit.spareBarrierArm);
                    cmd.Parameters.AddWithValue("@BUILDING_TENACY", visit.buildingTenancy);
                    cmd.Parameters.AddWithValue("@EQPT_CLEANL", visit.equipmentCleanliness);
                    cmd.Parameters.AddWithValue("@COP", visit.copBookNo);
                    cmd.Parameters.AddWithValue("@SEASON_TERM", visit.seasonTerminations);
                    cmd.Parameters.AddWithValue("@NEW_SEASON_APP", visit.newSeasonTerminations);
                    cmd.Parameters.AddWithValue("@REFUND_FORMS", visit.refundForms);
                    cmd.Parameters.AddWithValue("@RECORDING_STAT", visit.recordingStatus);
                    cmd.Parameters.AddWithValue("@CCTV_CAM", visit.cctvCamera);
                    cmd.Parameters.AddWithValue("@TIME_SYN_PMS", visit.timeSynPms);
                    cmd.Parameters.AddWithValue("@EARLIEST_FOOTAGE", visit.earliestDateOfFootage);
                    cmd.Parameters.AddWithValue("@TOT_BAR_RAISE", visit.totalBarrierRaise);
                    cmd.Parameters.AddWithValue("@RECEIPT_CHECKS", visit.receiptChecks);
                    cmd.Parameters.AddWithValue("@CONT_ON_LOC", visit.containersOnLoc);
                    cmd.Parameters.AddWithValue("@ABAND_VEHICLE", visit.abandVehicle);
                    cmd.Parameters.AddWithValue("@SURRO_CHANGES", visit.surroundingChanges);
                    cmd.Parameters.AddWithValue("@REMARKS", visit.remarks);
                    cmd.Parameters.AddWithValue("@UPDATEDBY", visit.updatedby);
                    if (isUpdate) { 
                        cmd.Parameters.AddWithValue("@jobid", visit.jobid);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@CREATEDBY", visit.createdby);
                        cmd.Parameters.AddWithValue("@TIME", visit.time);
                    }
                   Object indentityID = cmd.ExecuteScalar();
                   con.Close();
                   return indentityID;
                }
            }

        }


        public DataSet getCpcodeDataSet()
        {
          
            string sSQL = "SELECT CPCODE,CPNAME FROM [CarParkCentral].[dbo].[CarParkDetails]";
            sqlConnection = new SqlConnection(sConString);
            SqlDataAdapter cmd = new SqlDataAdapter(sSQL, sqlConnection);
            DataSet ds = new DataSet();
            cmd.Fill(ds, "CarParkCentral");

            return ds;
        }


        public SqlDataReader getVisitData()
        {
            string sSQL = "SELECT JOBID,CREATEDBY,CPCODE,DATE,TIME FROM [CarParkCentral].[dbo].[CarParkJobVisits] ORDER BY DATE DESC, TIME DESC";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            return reader;     
        }

        public SqlDataReader getDefectData()
        {
            string sSQL = "SELECT JOBID,CREATEDBY,CPCODE,DATE,TIME FROM [CarParkCentral].[dbo].[CarParkJobDefects] ORDER BY DATE DESC, TIME DESC";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            return reader;
        }

        public Visit getVisitDataByID(String jobid)
        {
            string sSQL = "SELECT * FROM [CarParkCentral].[dbo].[CarParkJobVisits] WHERE JOBID = @jobid";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dReader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@jobid", jobid);
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            dReader = cmd.ExecuteReader();

            Visit visit = new Visit();
            while (dReader.Read())
            {
                visit.jobid =  dReader["JOBID"].ToString();
                visit.abandVehicle = dReader["ABAND_VEHICLE"].ToString();
                visit.buildingTenancy = dReader["BUILDING_TENACY"].ToString();
                visit.carInPark = dReader["CARINPARK"].ToString();
                visit.cctvCamera = dReader["CCTV_CAM"].ToString();
                visit.containersOnLoc = dReader["CONT_ON_LOC"].ToString();
                visit.copBookNo = dReader["COP"].ToString();
                visit.createdby = dReader["CREATEDBY"].ToString();

                DateTime dtDate = dtDate = (DateTime)dReader["DATE"];
                visit.date = dtDate.ToString("dd MMM yyyy");

                visit.earliestDateOfFootage = dReader["EARLIEST_FOOTAGE"].ToString();
                visit.equipmentCleanliness = dReader["EQPT_CLEANL"].ToString();
                visit.intercom = dReader["INTERCOM"].ToString();
                visit.newSeasonTerminations = dReader["NEW_SEASON_APP"].ToString();
                visit.operationExec = dReader["CREATEDBY"].ToString();
                visit.receiptChecks = dReader["RECEIPT_CHECKS"].ToString();
                visit.recordingStatus = dReader["RECORDING_STAT"].ToString();
                visit.refundForms = dReader["REFUND_FORMS"].ToString();
                visit.remarks = dReader["REMARKS"].ToString();
                visit.seasonTerminations = dReader["SEASON_TERM"].ToString();
                visit.site = dReader["CPCODE"].ToString();
                visit.spareBarrierArm = dReader["SPARE_BARRIER_ARM"].ToString();
                visit.surroundingChanges = dReader["SURRO_CHANGES"].ToString();
                DateTime dtTime = DateTime.Parse(dReader["TIME"].ToString(), CultureInfo.InvariantCulture);
                visit.time = dtTime.ToString("hh:mm tt");
                visit.timeSynPms = dReader["TIME_SYN_PMS"].ToString();
                visit.totalBarrierRaise = dReader["TOT_BAR_RAISE"].ToString();
                visit.updatedby = dReader["UPDATEDBY"].ToString();

                dtDate = (DateTime)dReader["LASTUPDATE"];
                visit.lastupdate = dtDate.ToString("dd MMM yyyy");
            }
            return visit;
        }

        public Defects getDefectDataByID(String jobid)
        {
            string sSQL = "SELECT * FROM [CarParkCentral].[dbo].[CarParkJobDefects] WHERE JOBID = @jobid";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dReader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@jobid", jobid);
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            dReader = cmd.ExecuteReader();

            Defects defects = new Defects();
            while (dReader.Read())
            {
                defects.jobid = dReader["JOBID"].ToString();
                defects.acpipe = dReader["AC_DRAINAGE_PIPE"].ToString();
                defects.barrierKiosk = dReader["BARRIER_KIOSKS"].ToString();
                defects.barrierPadding = dReader["BARRIER_PADDING_ARM"].ToString();
                defects.bollards = dReader["BOLLARDS"].ToString();
                defects.boothCondition = dReader["BOOTH_CONDITION"].ToString();
                defects.boothFlooring = dReader["BOOTH_FLOORING"].ToString();
                defects.boothWrap = dReader["BOOTH_WRAP"].ToString();
                defects.cableTie = dReader["CABLE_TIE"].ToString();
                defects.cardReader = dReader["CARD_READER"].ToString();
                defects.carParkLights = dReader["CARpARK_LIGHTS"].ToString();
                defects.cctv = dReader["CCTV"].ToString();
                defects.site = dReader["CPCODE"].ToString();
                defects.createdby = dReader["CREATEDBY"].ToString();
                defects.dmgSurface = dReader["DAMAGE_SURFACE"].ToString();

                DateTime dtDate = dtDate = (DateTime)dReader["DATE"];

                defects.date = dtDate.ToString("dd MMM yyyy");
                defects.equipmentCleanliness = dReader["EQUIPMENT_CLEANLINESS"].ToString();
                defects.fadedPaintWorks = dReader["FADED_PAINTWORKS"].ToString();
                defects.generalCleanliness = dReader["GEN_CLEANLINES"].ToString();

                dtDate = dtDate = (DateTime)dReader["LASTUPDATE"];

                defects.lastupdate = dtDate.ToString("dd MMM yyyy");
                defects.lightBox = dReader["LIGHT_BOX"].ToString();
                defects.oilLeak = dReader["OIL_LEAK"].ToString();
                defects.operationExec = dReader["CREATEDBY"].ToString();
                defects.pgsSystem = dReader["PGS_SYSTEM"].ToString();
                defects.potHoles = dReader["POT_HOLES"].ToString();
                defects.safetyConcerns = dReader["SAFETY_CONCERNS"].ToString();
                defects.scanningIssues = dReader["SCANNINGISSUES"].ToString();
                defects.scrapbarrier = dReader["SCRAP_BARRIER"].ToString();
                defects.sign = dReader["SIGNS"].ToString();
                defects.spotLight = dReader["SPOT_LIGHT"].ToString();
                defects.systemKiosk = dReader["SYSTEM_KIOSKS"].ToString();
                DateTime dtTime = DateTime.Parse(dReader["TIME"].ToString(), CultureInfo.InvariantCulture);
                defects.time = dtTime.ToString("hh:mm tt");
                defects.updatedby = dReader["UPDATEDBY"].ToString();
                defects.waterPounding = dReader["WATER_POUNDING"].ToString();
                defects.waterPoundingcpl = dReader["WATERPOUNDING_CPL"].ToString();
                defects.remarks = dReader["OTHER_DEFECTS"].ToString();
            }
            return defects;
        }

        public void deleteVisitData(String jobid)
        {
            string sSQL = "DELETE FROM [CarParkCentral].[dbo].[CarParkJobVisits] WHERE JOBID=@jobid";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@jobid", jobid);
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            cmd.ExecuteReader();
            closeSQLConn();
        }

        public void deleteDefectData(String jobid)
        {
            string sSQL = "DELETE FROM [CarParkCentral].[dbo].[CarParkJobDefects] WHERE JOBID=@jobid";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@jobid", jobid);
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            cmd.ExecuteReader();
            closeSQLConn();
        }

        public bool getUser(String username)
        {
            string sSQL = "SELECT * FROM [Users].[dbo].[SITE_ALLOWED_USERS] WHERE username = @username AND SITE_ID='CPCHKLIST' AND IS_LOCKED='N'";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dReader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            dReader = cmd.ExecuteReader();

            
            while (dReader.Read())
            {
                return true;
            }
            return false;
        }

        public UserSession getUserToken(String auth)
        {
            string sSQL = "SELECT * FROM [Users].[dbo].[USER_SESSION]"+
                          "WHERE DATEDIFF(ss,REQUESTTIMESTAMP,CURRENT_TIMESTAMP) <= CONVERT(INT, TTL)" +
                          "AND TOKEN = @auth";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dReader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@auth", auth);
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            dReader = cmd.ExecuteReader();

            UserSession sess = null;
            while (dReader.Read())
            {
                sess= new UserSession();
                sess.IP = dReader["IPADD"].ToString();
                sess.RequestBy = dReader["REQUESTBY"].ToString();
                sess.RequestTime = dReader["REQUESTTIMESTAMP"].ToString();
                sess.Token = dReader["TOKEN"].ToString();
                sess.TTL = dReader["TTL"].ToString(); 
            }
            closeSQLConn();
            return sess;
           
        }

        public void addUser(User user)
        {
            string sSQL = "INSERT INTO [Users].[dbo].[UserDetails] VALUES (@username, @password, @email,'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "', @roleid, @salt)";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@username", user.username);
            cmd.Parameters.AddWithValue("@password", user.password);
            cmd.Parameters.AddWithValue("@email", user.email);
            cmd.Parameters.AddWithValue("@roleid", user.roleid);
            cmd.Parameters.AddWithValue("@salt", user.salt);
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            cmd.ExecuteReader();
            closeSQLConn();
        }

        public void closeSQLConn()
        {
            sqlConnection.Close();
        }


        
    }
}
