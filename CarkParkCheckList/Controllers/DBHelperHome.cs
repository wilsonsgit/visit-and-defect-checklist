﻿using CarParkCheckList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Web.Http;
using System.Configuration;
using System.Globalization;
using CarkParkCheckList.Models;

namespace CarParkCheckList.Controllers
{
    public class DBHelperHome : ApiController
    {
        private static String sConString = ConfigurationManager.ConnectionStrings["CarparkCentralConnectionString"].ToString();
        private SqlConnection sqlConnection;



        public string getTotalVisit()
        {
            string sSQL = "SELECT COUNT(DATE) AS NUM FROM [CarParkCentral].[dbo].[CarParkJobVisits] WHERE DATE = CONVERT(DATE,GETDATE())";
            string str = "0";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();


            if(reader.HasRows){
                while(reader.Read()){
                    str = reader["NUM"].ToString();
                    break;
                }
            }
            closeSQLConn();
            return str;
        }


        public string getTotalDefect()
        {
            string sSQL = "SELECT COUNT(DATE) AS NUM FROM [CarParkCentral].[dbo].[CarParkJobDefects] WHERE DATE = CONVERT(DATE,GETDATE())";
            string str = "0";
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    str = reader["NUM"].ToString();
                    break;
                }
            }
            closeSQLConn();
            return str;
        }

        public List<ActivityLog> getLogPeek(char top)
        {
            String limit = top == '*' ? " " : " TOP " + top + " ";
            List<ActivityLog> activityList = new List<ActivityLog>();
            string sSQL =
                "SELECT" + limit + "CREATEDBY, DATETIME , DATEDIFF(SS, DATETIME, GETDATE()) AS DIFFSEC, SOURCE FROM( " +
                    "SELECT CREATEDBY, CONVERT(varchar, DATE) + ' ' +CONVERT(varchar, TIME) AS DATETIME , 'defect' AS SOURCE FROM [CarParkCentral].[dbo].[CarParkJobDefects] " +
	                "UNION ALL "+
	                "SELECT CREATEDBY, CONVERT(varchar, DATE) + ' ' +CONVERT(varchar, TIME) AS DATETIME, 'visit' AS SOURCE FROM [CarParkCentral].[dbo].[CarParkJobVisits] "+
	                "UNION ALL "+
	                "SELECT CREATEDBY, DATECREATED AS DATETIME, 'site_allowed_users' AS SOURCE FROM [Users].[dbo].[SITE_ALLOWED_USERS] "+
	                "UNION ALL "+
	                "SELECT CREATEDBY, DATETIMECREATED AS DATETIME, 'site_permissions' AS SOURCE FROM [Users].[dbo].[SITES_PERMISSION] "+
	                "UNION ALL "+
	                "SELECT CREATEDBY, DATECREATED AS DATETIME, 'user_role' AS SOURCE FROM [Users].[dbo].[USER_ROLE] "+
	                "UNION ALL "+
	                "SELECT [REQUESTBY], REQUESTTIMESTAMP AS DATETIME, 'login_request' AS SOURCE FROM [Users].[dbo].[USER_SESSION] "+
                ")AS A "+
                "WHERE DATETIME IS NOT NULL "+
                "ORDER BY DATETIME DESC";
      
            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();


            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ActivityLog alog = new ActivityLog();
                    alog.user = reader["CREATEDBY"].ToString();
                    alog.action = reader["SOURCE"].ToString();
                    alog.datetime = reader["DATETIME"].ToString();
                    alog.timelapse = reader["DIFFSEC"].ToString();
                    activityList.Add(alog);
                }
            }
            closeSQLConn();
            return activityList;
        }

        public SqlDataReader getVisitStat(int type)
        {
            string sSQL = "";
            if(type==0)
                 sSQL = 
                    "Declare @FromDate   Date, @EndDate date, @DSQL varchar(4000); " +
                    "SET  @EndDate   =   GETDATE(); " +
                    "SET  @FromDate   =   DATEADD(DAY, -7 ,GETDATE()); " +
                    "SET @DSQL = ''; " +
                    "while @FromDate<=@EndDate " +
                    "Begin " +
                        "SET @DSQL = @DSQL + ' SELECT COUNT(*) AS TOTAL, '''  + Cast(@FromDate as varchar) + ''' AS DATE FROM " + "[CarParkCentral].[dbo].[CarParkJobVisits] WHERE DATE = '''+ Cast(@FromDate as varchar) + ''''; " +
                        "IF @FromDate < @EndDate " +
                        "SET @DSQL = @DSQL + ' UNION ALL'; " +
                        "set @FromDate = DATEADD(DAY, 1, @FromDate) " +
                    "End " +
                    "SET @DSQL = 'SELECT CONVERT(varchar, CAST(S.DATE AS DATE), 106) AS DATE, TOTAL FROM(' + @DSQL + ')S' " +
                    "PRINT(@DSQL) " +
                    "EXEC(@DSQL)";
            else
                  sSQL =
                      "Declare @FromDate   Date, @EndDate date, @DSQL varchar(4000); " +
                      "SET  @EndDate   =   GETDATE(); " +
                      "SET  @FromDate   =   DATEADD(MONTH, -6 ,GETDATE());  " +
                      "SET @DSQL = ''; " +
                      "while @FromDate<=@EndDate " +
                      "Begin " +
                          "SET @DSQL = @DSQL + ' SELECT COUNT(*) AS TOTAL, '''  + Cast(@FromDate as varchar) + ''' AS DATE FROM [CarParkCentral].[dbo].[CarParkJobVisits] WHERE MONTH('''+ Cast(@FromDate as varchar) + ''') = MONTH(DATE)'; " +
                          "IF @FromDate < @EndDate " +
                          "SET @DSQL = @DSQL + ' UNION ALL'; " +
                          "set @FromDate = DATEADD(MONTH, 1, @FromDate) " +
                      "End " +
                      "SET @DSQL = 'SELECT CONVERT(varchar(4), CAST(S.DATE AS DATE),100) + CONVERT(varchar(4), CAST(S.DATE AS DATE))  AS DATE, TOTAL FROM(' + @DSQL + ')S' " +
                      "PRINT(@DSQL) " +
                      "EXEC(@DSQL)";


            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            return reader;
        }
        public SqlDataReader getDefectStat(int type)
        {
              string sSQL ="";
              if (type == 0)
                  sSQL =
                     "Declare @FromDate   Date, @EndDate date, @DSQL varchar(4000); " +
                     "SET  @EndDate   =   GETDATE(); " +
                     "SET  @FromDate   =   DATEADD(DAY, -7 ,GETDATE()); " +
                     "SET @DSQL = ''; " +
                     "while @FromDate<=@EndDate " +
                     "Begin " +
                         "SET @DSQL = @DSQL + ' SELECT COUNT(*) AS TOTAL, '''  + Cast(@FromDate as varchar) + ''' AS DATE FROM " + "[CarParkCentral].[dbo].[CarParkJobDefects] WHERE DATE = '''+ Cast(@FromDate as varchar) + ''''; " +
                         "IF @FromDate < @EndDate " +
                         "SET @DSQL = @DSQL + ' UNION ALL'; " +
                         "set @FromDate = DATEADD(DAY, 1, @FromDate) " +
                     "End " +
                     "SET @DSQL = 'SELECT CONVERT(varchar, CAST(S.DATE AS DATE), 106) AS DATE, TOTAL FROM(' + @DSQL + ')S' " +
                     "PRINT(@DSQL) " +
                     "EXEC(@DSQL)";
              else
                  sSQL =
                      "Declare @FromDate   Date, @EndDate date, @DSQL varchar(4000); " +
                      "SET  @EndDate   =   GETDATE(); " +
                      "SET  @FromDate   =   DATEADD(MONTH, -6 ,GETDATE());  " +
                      "SET @DSQL = ''; " +
                      "while @FromDate<=@EndDate " +
                      "Begin " +
                          "SET @DSQL = @DSQL + ' SELECT COUNT(*) AS TOTAL, '''  + Cast(@FromDate as varchar) + ''' AS DATE FROM [CarParkCentral].[dbo].[CarParkJobDefects] WHERE MONTH('''+ Cast(@FromDate as varchar) + ''') = MONTH(DATE)'; " +
                          "IF @FromDate < @EndDate " +
                          "SET @DSQL = @DSQL + ' UNION ALL'; " +
                          "set @FromDate = DATEADD(MONTH, 1, @FromDate) " +
                      "End " +
                      "SET @DSQL = 'SELECT CONVERT(varchar(4), CAST(S.DATE AS DATE),100) + CONVERT(varchar(4), CAST(S.DATE AS DATE))  AS DATE, TOTAL FROM(' + @DSQL + ')S' " +
                      "PRINT(@DSQL) " +
                      "EXEC(@DSQL)";


            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            return reader;
        }

        public void closeSQLConn()
        {
            sqlConnection.Close();
        }
    }
}
