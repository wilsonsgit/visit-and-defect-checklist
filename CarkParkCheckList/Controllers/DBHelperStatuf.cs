﻿using CarParkCheckList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Web.Http;
using System.Configuration;
using System.Globalization;
using CarkParkCheckList.Models;

namespace CarParkCheckList.Controllers
{
    public class DBHelperStatuf : ApiController
    {
        private static String sConString = ConfigurationManager.ConnectionStrings["CarparkCentralConnectionString"].ToString();
        private SqlConnection sqlConnection;

        public SqlDataReader getStat()
        {
            string sSQL =
                "Declare @cols NVARCHAR (MAX),@month int, @year int, @query NVARCHAR(MAX) " +
                "set @month = MONTH(GETDATE()) " +
                "set @year = YEAR(GETDATE())  " +
                ";WITH  " +
                "datelist AS( " +
                    "SELECT " +
                        "CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number AS DATE " +
                    "FROM master..spt_values " +
                    "WHERE type = 'P' AND " +
                    "(CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number )< " +
                    "DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) ) " +
                ") " +
                "SELECT @cols = COALESCE (@cols + ',[' + CONVERT(NVARCHAR, [DATE], 110) + ']',  " +
                               "'[' + CONVERT(NVARCHAR, [DATE], 110) + ']') " +
                               "FROM    ( " +
                                        "SELECT Distinct(DATE) as DATE " +
                                        "FROM datelist " +
                                        ") PV   " +
                                        "ORDER BY [DATE] " +
                "SET @query = ' " +
                             "SELECT A.USERNAME, s.* FROM ( " +
                                 "SELECT  * FROM  " +
                                 "( " +
                                     "SELECT DATE, COUNT(*) AS  NOOFRECORDS, CREATEDBY  " +
                                     "FROM [CarParkCentral].[dbo].[CarParkJobVisits]  " +
                                     "GROUP BY CREATEDBY,DATE " +
                                 ") x " +
                                 "PIVOT  " +
                                 "( " +
                                     "SUM(NOOFRECORDS) " +
                                     "FOR [DATE] IN (' + @cols + ') " +
                                ") p    " +
                            ")s " +
                            "RIGHT JOIN [Users].[dbo].[SITE_ALLOWED_USERS] A ON A.USERNAME = S.CREATEDBY " +
                            "' " +
                "EXEC SP_EXECUTESQL @query ";

            sqlConnection = new SqlConnection(sConString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = sSQL;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            reader = cmd.ExecuteReader();
            return reader;
        }

        public void closeSQLConn()
        {
            sqlConnection.Close();
        }
    }
}
