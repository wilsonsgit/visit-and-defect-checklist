﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CarkParkCheckListVisit._Default" MasterPageFile="~/CarParkCheckList.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server" class="field-container">
    <div class="container-fluid">
        <div class="row container-custom ">
            <div class="panel-body">
                <a href="./form.aspx" class="btn btn-success" role="button">Add New Data</a>
            </div>
            <div class="col-lg-12">
                <div class="intro-message">
                    <table id="listContainer" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
                            <th>Action</th>
							<th>Operation Executive</th>
							<th>Site</th>
							<th>Date</th>
							<th>Time</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th>Action</th>
							<th>Operation Executive</th>
							<th>Site</th>
							<th>Date</th>
							<th>Time</th>
						</tr>
					</tfoot>
					<tbody>
                        <%Response.Write(renderTableView()); %>                        
					</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>