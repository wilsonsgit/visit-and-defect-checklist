﻿
//JQuery Functions

$(document).ready(function () {

    // var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    $('#listContainer').DataTable({
           saveState: true
    });
    var time_input = $('.txtTime');
    time_input.prop('disabled', true);
   
    var date_input = $('.txtDate');
    date_input.prop('disabled', true);

    var opexec_input = $('.txtOperationExacutive');
    opexec_input.prop('disabled', true);
    

    $('.txtSelect').selectize();
    var control = $('.txtSelect')[0].selectize;
    if (!checkQryString('q') && !checkQryString('m')) {
        control.clear();
    }


    $('.txtShowCal').datepicker({
        autoclose: true,
        viewMode : 'years',
        format: "yyyy-mm-dd"
       
    });

    var x = $(".file-input");
    for (i = 1; i <= 3; i++) {
        var img_inpt = "#bodyContainer_imageInput" + i + "_view";        
        if ($(img_inpt).length) {
           $(x.get(i - 1)).hide();
        }
    }



});

function showModal(uri) {
    $('#imagepreview').attr('src', $('#bodyContainer_image'+uri).attr('src'));
       $('#imagemodal').modal('show'); 
}



function checkQryString(field) {
    var url = window.location.href;
    if (url.indexOf('?' + field + '=') != -1)
        return true;
    else if (url.indexOf('&' + field + '=') != -1)
        return true;
    return false
}
function deleteImg(id) {
   
    var res = confirm("Are you sure you want to delete this image attachment?");
    var x = $(".file-input");
    if (res) {
        var y = $("#bodyContainer_d"+id).val(1);
        var img_cont = ".imageInput" + id + "_view";
        $(img_cont).hide();
        $(x.get(parseInt(id) - 1)).show();

    }
    
}
function deleteView(id) {
    var res = confirm("Are you sure you want to delete this row?");

    if (res) {
        window.location.replace("./?d=" + id);
    }
}


$('#collapse1').on('show.bs.collapse', function () {
    $('#Glyp-Col1').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
});
$('#collapse1').on('hide.bs.collapse', function () {
    $('#Glyp-Col1').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
});
$('#collapse2').on('show.bs.collapse', function () {
    $('#Glyp-Col2').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
});
$('#collapse2').on('hide.bs.collapse', function () {
    $('#Glyp-Col2').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
});
$('#collapse3').on('show.bs.collapse', function () {
    $('#Glyp-Col3').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
});
$('#collapse3').on('hide.bs.collapse', function () {
    $('#Glyp-Col3').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
});
$('#collapse4').on('show.bs.collapse', function () {
    $('#Glyp-Col4').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
});
$('#collapse4').on('hide.bs.collapse', function () {
    $('#Glyp-Col4').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
});
$('#collapse5').on('show.bs.collapse', function () {
    $('#Glyp-Col5').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
});
$('#collapse5').on('hide.bs.collapse', function () {
    $('#Glyp-Col5').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
});
$('#collapse6').on('show.bs.collapse', function () {
    $('#Glyp-Col5').removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
});
$('#collapse6').on('hide.bs.collapse', function () {
    $('#Glyp-Col5').removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
});

//JS










