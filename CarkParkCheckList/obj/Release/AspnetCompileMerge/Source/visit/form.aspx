﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="form.aspx.cs" Inherits="CarParkCheckList.VisitForm._Default" MasterPageFile="~/CarParkCheckList.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="messageContainer" runat="server">
    <%
        if (Session["INFO_MSG"] != null)
        {
            Response.Write(Session["INFO_MSG"]);
            Session["INFO_MSG"] = null;
        }
          
     %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server">
    <form runat="server">
        <div class="form-group">
            <div class="panel-group" id="accordion">
                <div  class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="javascript:history.back(-1)" class="panel-title panel-title pull-left"><span class='glyphicon glyphicon-chevron-left'></span>&nbsp;Back</a>
                            <h4 class="panel-title panel-title-center" >
                               <strong>Check List Visit Form</strong>
                            </h4>
                        </div>     
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                               Car Park Visit Details
                            </h5>
                        </div>
                        <div class="panel-body">
                            <div  class="col-md-2">
                                <label class="control-label" for="date">Date</label>
                                <div class="input-group">
                                       <asp:TextBox ID="txtDate" type="text" class="form-control txtDate"  runat="server" ReadOnly></asp:TextBox>
                                       <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>

                             <div  class="col-md-2">
                                <label class="control-label" for="time">Time</label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <asp:TextBox ID="txtTime" type="text" class="form-control txtTime"  runat="server" ReadOnly></asp:TextBox>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                </div>
                            </div>

                             <div  class="col-md-4">
                                <label>Operation Executive</label>
                                <asp:TextBox ID="txtOperationExacutive" type="text" class="form-control txtOperationExacutive"  runat="server" ReadOnly></asp:TextBox>
                            </div>
                            
                            <div  class="col-md-4">
                                <label>Site</label>
                                <asp:DropDownList runat="server"  ID="txtSite" class="demo-default txtSelect" placeholder="Select a site..." required></asp:DropDownList>
                            </div>

                        </div>

                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse1">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col1" class="glyphicon glyphicon-chevron-down"></span></span>
                                    General Car Park Surrounding
                                </a>
                            </h4>
                        </div>

                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label1" class="control-label" runat="server" Text="Car-In-Park"></asp:Label>
                                    <asp:TextBox ID="txtCarInPark" type="number" class="form-control input"  runat="server" Rows="3"
                                         min="0" step="1" data-bind="value:bodyContainer_txtCarInPark" Text="0"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                   <asp:Label ID="Label2" class="control-label" runat="server" Text="Intercom"></asp:Label>
                                   <asp:DropDownList ID="txtIntercom" runat="server" class="demo-default txtSelect" ></asp:DropDownList>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label3" class="control-label" runat="server" Text="Spare Barrier Arm"></asp:Label>
                                    <asp:TextBox ID="txtSpareBarrierArm" type="number" class="form-control input"  runat="server"
                                       max="10" min="0" step="1"  data-bind="value:bodyContainer_txtSpareBarrierArm" Text="0"></asp:TextBox>
                               </div>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label4" class="control-label" runat="server" Text="Building Tenancy (%)"></asp:Label>
                                    <asp:TextBox ID="txtBuildingTenancy" type="number" class="form-control"  runat="server"
                                        min="0" max="100" step="1" Text="0" ata-bind="value:bodyContainer_txtBuildingTenancy"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="Label5" class="control-label" runat="server" Text="Equipment Cleanliness"></asp:Label>
                                    <asp:DropDownList ID="txtEquipmentCleanliness" runat="server" class="demo-default txtSelect" ></asp:DropDownList>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse2">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col2" class="glyphicon glyphicon-chevron-down"></span></span>
                                     Season Applications
                                </a>
                             </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label13" class="control-label" runat="server" Text="COP Book Number and last used page"></asp:Label>
                                    <asp:TextBox ID="txtCopBook" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label14" class="control-label" runat="server" Text="Season Terminations"></asp:Label>
                                    <asp:TextBox ID="txtSeasonTerm" type="number" class="form-control"  runat="server" Text="0"
                                        min="0" max="30" step="1" data-bind="value:bodyContainer_txtSeasonTerm"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label15" class="control-label" runat="server" Text="New Season Applications"></asp:Label>
                                    <asp:TextBox ID="txtNewSeasonApp" type="number" class="form-control"  runat="server" Text="0"
                                        min="0" max="30" step="1" data-bind="value:bodyContainer_txtNewSeasonApp"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label16" class="control-label" runat="server" Text="Refund Forms "></asp:Label>
                                    <Textarea class="form-control" rows="5" ID="txtRefundForms" runat="server"></Textarea>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse3">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col3" class="glyphicon glyphicon-chevron-down"></span></span>
                                    Surveillance Camera
                                </a>
                             </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                   <asp:Label ID="Label21" class="control-label" runat="server" Text="Earliest Date of Footage"></asp:Label>
                                    <asp:TextBox ID="txtEarliestDateFootage" type="text" class="form-control txtShowCal"  runat="server"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label19" class="control-label" runat="server" Text="Recording Status"></asp:Label>
                                    <asp:DropDownList ID="txtRecordingStat" runat="server" class="demo-default txtSelect" ></asp:DropDownList>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label20" class="control-label" runat="server" Text="Time Syn with PMS"></asp:Label>
                                   <asp:DropDownList ID="txtTimeSynPMS" runat="server" class="demo-default txtSelect" ></asp:DropDownList>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label18" class="control-label" runat="server" Text="CCTV Camera"></asp:Label>
                                    <Textarea class="form-control" rows="5" ID="txtcctv" runat="server"></Textarea>
                               </div>
                            </div>  
                        </div>
                    </div>
            
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse5">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col5" class="glyphicon glyphicon-chevron-down"></span></span>
                                    Any Other
                                </a>
                             </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label6" class="control-label" runat="server" Text="Total Barrier Raise "></asp:Label>
                                    <asp:TextBox ID="txtTotalBarRaise" type="number" class="form-control"  runat="server" Text="0" step="1" min="0"></asp:TextBox>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label7" class="control-label" runat="server" Text="Receipt Checks (Exit and Top-up Terminal)"></asp:Label>
                                    <asp:DropDownList ID="txtReceiptChecks" runat="server" class="demo-default txtSelect" ></asp:DropDownList>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label8" class="control-label" runat="server" Text="Containers on Location during visit / details of owner and any discrepancy found"></asp:Label>
                                    <asp:TextBox ID="txtContainersOnLocation" type="text" class="form-control"  runat="server"></asp:TextBox>
                               </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <asp:Label ID="Label9" class="control-label" runat="server" Text="Abandoned Vehicle"></asp:Label>
                                    <Textarea class="form-control" rows="5" ID="txtAbandonedVehicle" runat="server"></Textarea>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label10" class="control-label" runat="server" Text="Surrounding changes that may impact revenue"></asp:Label>
                                    <Textarea class="form-control" rows="5" ID="txtSurroundinChanges" runat="server"></Textarea>
                               </div>
                               <div class="col-md-4">
                                    <asp:Label ID="Label11" class="control-label" runat="server" Text="Any Other Input and Recommendation for improvements on revenue and general look"></asp:Label>
                                    <Textarea class="form-control" rows="4" ID="txtAnyOther" runat="server"></Textarea>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" data-toggle="collapse" href="#collapse4">
                                <a>
                                    <span class="pull-right"><span id="Glyp-Col5" class="glyphicon glyphicon-chevron-down"></span></span>
                                    Image Attachment
                                </a>
                             </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <input ID="imageInput1"  type="file" class="file file-loading imageInput" data-allowed-file-extensions='["png", "jpg", "jpeg"]' runat="server"/>
                                    <asp:HiddenField ID="d1" runat="server" value="0"/>
                                    <div class="input-group imageInput1_view"  runat="server" ID="imageInput1_view" visible="false">
                                        <asp:Image ID="image1" class="infoImgThumb img-rounded"  runat="server" Width="304" Height="236" ImageUrl="../UploadedImage/54/Jellyfish.jpg"/>
                                        <a href='javascript:deleteImg(1)' class="btn btn-danger">DELETE</a>
                                    </div>
                               </div>
                               <div class="col-md-4">
                                    <input ID="imageInput2" type="file" class="file file-loading imageInput" data-allowed-file-extensions='["png", "jpg", "jpeg"]' runat="server"/>
                                    <asp:HiddenField ID="d2" runat="server" Value="0" />
                                    <div class="input-group imageInput2_view"  runat="server" ID="imageInput2_view" visible="false">
                                        <asp:Image ID="image2" class="infoImgThumb img-rounded"  runat="server" Width="304" Height="236" ImageUrl="../UploadedImage/54/Jellyfish.jpg"/>
                                        <a href='javascript:deleteImg(2)' class="btn btn-danger">DELETE</a>
                                    </div>
                               </div>
                               <div class="col-md-4">
                                    <input ID="imageInput3" type="file" class="file file-loading imageInput" data-allowed-file-extensions='["png", "jpg", "jpeg"]' runat="server"/>
                                    <asp:HiddenField ID="d3" runat="server" Value="0" />
                                    <div class="input-group imageInput3_view" runat="server" ID="imageInput3_view" visible="false">
                                        <asp:Image ID="image3" class="infoImgThumb img-rounded"  runat="server" Width="304" Height="236" ImageUrl="../UploadedImage/54/Jellyfish.jpg"/>
                                        <a href='javascript:deleteImg(3)' class="btn btn-danger">DELETE</a>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <asp:Button ID="formButton" class="btn btn-success btn-lg btn-block" runat="server" Text="Save" OnClick="btnListener"/>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>