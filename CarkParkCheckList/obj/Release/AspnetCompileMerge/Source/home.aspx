﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarParkCheckList.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="CarPark.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="messageContainer" runat="server">
    <%
        if (Session["INFO_MSG"] != null)
        {
            Response.Write(Session["INFO_MSG"]);
            Session["INFO_MSG"] = null;
        }
          
     %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server">
    <div class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                       <h1>Wilson Parking</h1>
                         <h3>People who know the business</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
