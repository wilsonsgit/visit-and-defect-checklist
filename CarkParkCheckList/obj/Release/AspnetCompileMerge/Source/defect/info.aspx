﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="info.aspx.cs" Inherits="CarkParkCheckList.DefectsForm._Default" MasterPageFile="~/CarParkCheckList.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContainer" runat="server">
    <div class="container">
        <div class="panel-body"> 
            <a href="./" class="btn btn-info" role="button"><span class='glyphicon glyphicon-menu-left'></span>&nbsp;Back</a>
            <asp:panel class="btn-group pull-right" Visible="false" ID="infoAction" runat="server">
                <a href='<%Response.Write(copyLink);%>' class="btn btn-success">Copy</a>
                <a href='<%Response.Write(editLink);%>' class="btn btn-warning">Edit</a>
                <a href='#' class="btn btn-danger" <%Response.Write(deleteLink);%>>Delete</a>
            </asp:panel>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Car Park Visit Details </h4> 
            <div class="panel-body">
                 <div  class="col-md-3">
                        <div class="input-group">
                            <p><strong>Operation Executive:</strong> <%_out(defects.operationExec);%></p>
                        </div>
                 </div>
                 <div  class="col-md-3">
                        <div class="input-group">
                            <p><strong>Date:</strong> <%_out(defects.date);%></p>
                        </div>
                 </div>
                <div  class="col-md-3">
                        <div class="input-group">
                            <p><strong>Time:</strong> <%_out(defects.time);%></p>
                        </div>
                 </div>
                <div  class="col-md-3">
                    <div class="input-group">
                        <p><strong>Site:</strong> <%_out(defects.site);%></p>
                    </div>
                </div>  
            </div>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>General Car Park Surrounding </h4> 
            <div class="panel-body">
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Bollards:</strong>
                        <p><%_out(defects.bollards);%></p>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Equipment Cleanliness:</strong>
                        <p><%_out(defects.equipmentCleanliness);%></p>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Sign:</strong>
                        <p><%_out(defects.sign);%></p>
                    </div>
                 </div>
            </div>
            <div class="panel-body">
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Spotlight:</strong>
                        <p><%_out(defects.spotLight);%></p>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Light-Box:</strong>
                        <p><%_out(defects.lightBox);%></p>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Scrap Barrier:</strong>
                        <p><%_out(defects.scrapbarrier);%></p>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Safety Concerns:</strong>
                        <p><%_out(defects.safetyConcerns);%></p>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Car Park Lights:</strong>
                        <p><%_out(defects.carParkLights);%></p>
                    </div>
                </div>
                <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Pot Holes:</strong>
                        <p><%_out(defects.potHoles);%></p>
                    </div>
                </div>
            </div>
             <div class="panel-body">
                <div  class="col-md-6">
                    <div class="input-group">
                        <strong>General Cleanliness:</strong>
                        <p><%_out(defects.generalCleanliness);%></p>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Water Pounding:</strong>
                        <p><%_out(defects.waterPounding);%></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Car Park Lots</h4> 
            <div class="panel-body">
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Faded Paint Works:</strong>
                            <p><%_out(defects.fadedPaintWorks);%></p>
                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Oil Leak:</strong>
                            <p><%_out(defects.oilLeak);%></p>
                        </div>
                 </div>
                 <div  class="col-md-4">
                        <div class="input-group">
                            <strong>Water Pounding:</strong>
                            <p><%_out(defects.waterPoundingcpl);%></p>
                        </div>
                 </div>
            </div>
             <div class="panel-body">
                 <div  class="col-md-6">
                        <div class="input-group">
                            <strong>PGS System:</strong>
                            <p><%_out(defects.pgsSystem);%></p>
                        </div>
                 </div>
                 <div  class="col-md-6">
                        <div class="input-group">
                            <strong>Damage Surface:</strong>
                            <p><%_out(defects.dmgSurface);%></p>
                        </div>
                 </div>
            </div>
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Hardware</h4> 
            <div class="panel-body">
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Scanning Issues:</strong>
                        <p><%_out(defects.scanningIssues);%></p>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>Card Reader:</strong>
                        <p><%_out(defects.cardReader);%></p>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <strong>CCTV:</strong>
                        <p><%_out(defects.cctv);%></p>
                    </div>
                 </div>
            </div>
            <div class="panel-body">
                 <div  class="col-md-6">
                    <div class="input-group">
                        <strong>System Kiosk:</strong>
                        <p><%_out(defects.systemKiosk);%></p>
                    </div>
                 </div>
                 <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Barrier Kiosk:</strong>
                        <p><%_out(defects.barrierKiosk);%></p>
                    </div>
                 </div>
            </div>
            <div class="panel-body">
                 <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Barrier Padding / Barrier Arm </strong>
                        <p><%_out(defects.barrierPadding);%></p>
                    </div>
                 </div>
                 <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Cable Tie:</strong>
                        <p><%_out(defects.cableTie);%></p>
                    </div>
                 </div>
            </div>
         </div>
         <div class="bs-callout bs-callout-info"> 
            <h4>Car Park Booth </h4> 
            <div class="panel-body">
                 <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Aircon & Drainage Pipe:</strong>
                        <p><%_out(defects.acpipe);%></p>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Booth Condition:</strong>
                        <p><%_out(defects.boothCondition);%></p>
                    </div>
                </div>
            </div>  
            <div class="panel-body">
                 <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Booth Flooring:</strong>
                        <p><%_out(defects.boothFlooring);%></p>
                    </div>
                </div>
                <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Booth Wrap:</strong>
                        <p><%_out(defects.boothWrap);%></p>
                    </div>
                </div>
            </div>  
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Any Other Deffects</h4> 
            <div class="panel-body">
                    <div  class="col-md-6">
                    <div class="input-group">
                        <strong>Remarks:</strong>
                        <p><%_out(defects.remarks);%></p>
                    </div>
                </div>
            </div>  
        </div>
        <div class="bs-callout bs-callout-info"> 
            <h4>Image Attachment</h4> 

             <div class="panel-body">
                 <div  class="col-md-4">
                    <div class="input-group">
                        <a onclick="javascript:showModal(1)"><asp:Image ID="image1" class="infoImgThumb img-rounded img-fluid"  runat="server" Width="70%" Height="60%" Visible="false"/></a>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <a onclick="javascript:showModal(2)"><asp:Image ID="image2" class="img-rounded infoImgThumb img-fluid"  runat="server" Width="70%" Height="60%" Visible="false"/></a>
                    </div>
                 </div>
                 <div  class="col-md-4">
                    <div class="input-group">
                        <a onclick="javascript:showModal(3)"><asp:Image ID="image3" class="img-rounded infoImgThumb img-fluid"  runat="server" Width="70%" Height="60%" Visible="false"/></a>
                    </div>
                 </div>
            </div>
        </div>
        <p class="pull-right">Last Updated by: <%_out(defects.updatedby);%> | Last Updated on: <%_out(defects.lastupdate);%></p>
    </div>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Image Attachment Preview</h4>
                </div>
                <div class="modal-body">
                    <img src="" id="imagepreview" style="width: 100%; height: 100%" >
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>