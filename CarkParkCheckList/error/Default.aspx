﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CarkParkCheckList.Error.Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Car Park Checklist</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="~/img/favicon.ico" id="favicon"/>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="~/css/errorstyle.css"/>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
</head>
    <body>
        <div class="container">
            <div class="well">
                <h1><div class="ion ion-alert-circled"></div> Something went wrong!</h1>
                <p>An error occured, while proccessing your request.</p>
                <p>
                       <asp:HyperLink id="homelink" class="btn btn-default btn-lg" NavigateUrl="~/home.aspx" Text="Home" runat="server"/>
                </p>
            </div>
        </div>
    </body>
</html>