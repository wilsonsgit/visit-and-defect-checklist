﻿using CarParkCheckList.Controllers;
using CarParkCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarPark
{
    public partial class Home : System.Web.UI.Page
    {

        private int totVisit;
        private int totDefect;
        private List<ActivityLog> loglist;
        private SqlDataReader sqlGraphDataVisit;
        private SqlDataReader sqlGraphDataDefect;
        private DBHelperHome db;
        private int grapKind;

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            db = new DBHelperHome();
            loglist = db.getLogPeek('5');
            totVisit = Int32.Parse(db.getTotalVisit());
            totDefect = Int32.Parse(db.getTotalDefect());
           
            
            grapKind = 0;
            if (Request.QueryString["stat"] !=null)
            {
                grapKind = Int32.Parse(Request.QueryString["stat"]);
            }
            sqlGraphDataDefect = db.getDefectStat(grapKind);
            sqlGraphDataVisit = db.getVisitStat(grapKind);
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void getLogs()
        {
            String str = "";
            foreach(ActivityLog log in loglist){
                str += "<a href='#' class='list-group-item'>" +
                        "<i class='fa "+getIcon(log.action)+" fa-fw'></i> " +
                        "<span>"+ log.user +"</span>" +
                        "<span class='pull-right text-muted small'>" +
                            "<em>" + getTime (log.timelapse)+ "</em>" +
                        "</span>" +
                    "</a>";
            }
            Response.Write(str);
        }

        private string getIcon(string action)
        {
            switch (action)
            {
                case "login_request": return "fa-sign-in";
                case "user_role": return "fa-users";
                case "site_permissions": return "fa-certificate";
                case "defect": return "fa-wrench";
                case "visit": return "fa-paperclip";
                default:
                    return "fa-cog";
            }
        }

        private string getTime(String sec)
        {
            int elapse = Int32.Parse(sec);
            int min = 60;
            int hr = min * 60;
            int da = hr * 24;
            int mo = da * 30;
            int ye = mo * 12;

            if (elapse < min)
                return sec + " sec ago";
            else if (elapse < hr)
            {
                int mins = elapse / min;
                int secs = elapse % min;
                return mins +" min " +  (secs  > 0 ?  secs +" sec ago" : "ago");

            }else if(elapse < da){
                int hrs = elapse / hr;
                int mins = elapse % hr % min;
                return hrs + " hr " + (mins > 0 ? mins + " min ago" : "ago");
            }else if (elapse < mo)
            {
                int das = elapse / da;
                int hrs = elapse % da / hr ;
                return das + " da " + (hrs > 0 ? hrs + " hr ago" : "ago");
            }else if (elapse < ye)
            {
                int mos = elapse / mo;
                int das = elapse % mo / da;
                return mos + " mo " + (das > 0 ? das + " da ago" : "ago");
            }else
            {
                int yes = elapse / ye;
                int mos = elapse % ye / mo ;
                return yes + " mo " + (mos > 0 ? mos + " da ago" : "ago");
            }

        }
           


        protected void getTiles(string icon, object title, string link, string value, string theme)
        {
            Response.Write(
            "<div class='col-lg-3 col-md-6'>" +
                "<div class='panel panel-"+theme+"'>" +
                    "<div class='panel-heading'>" +
                        "<div class='row'>" +
                            "<div class='col-xs-3'>" +
                                "<i class='fa fa-"+icon+" fa-5x'></i>" +
                            "</div>" +
                            "<div class='col-xs-9 text-right'>" +
                                "<div class='huge'>"+title+"</div>" +
                                "<div>"+value+"</div>" +
                            "</div>" +
                        "</div>" +
                    "</div>" +
                    "<a href='"+link+"'>" +
                        "<div class='panel-footer'>" +
                            "<span class='pull-left'>View Details</span>" +
                            "<span class='pull-right'><i class='glyphicon glyphicon-chevron-right'></i></span>" +
                            "<div class='clearfix'></div>" +
                        "</div>" +
                    "</a>" +
                "</div>" +
            "</div>");
        }

        public void plotGraphData()
        {
            String title;
            if (grapKind == 0)
                title = "7 Days";
            else
                title = "7 months";

            string cat = "[";
            string vdata = "[";
            string ddata = "[";
            if (sqlGraphDataDefect.HasRows)
            {
                while (sqlGraphDataDefect.Read())
                {
                    cat = cat + "'" + sqlGraphDataDefect["DATE"].ToString()  + "',";
                    ddata = ddata + sqlGraphDataDefect["TOTAL"].ToString() + ",";
                }
            }
            cat = cat + "]";
            ddata = ddata + "]";
            if (sqlGraphDataVisit.HasRows)
            {
                while (sqlGraphDataVisit.Read())
                {
                    vdata = vdata + sqlGraphDataVisit["TOTAL"].ToString() + ",";
                }
            }
            vdata = vdata + "]";
         
            string str = 
            ",title: { "+
                    "text: '" + title + " - Forms Stat' " +
            "}, "+
            "xAxis: { " +
                "categories: " + cat + ", " +
                "crosshair: true " +
            "}, " +
            "series: [{ " +
                "name: 'Visits', " +
                "data:" + vdata + 
            "}, { " +
                "name: 'Defects', " +
                "data: " + ddata +
            "}]";
            db.closeSQLConn();
            Response.Write(str);
        }

        public int getTotVisit()
        {
            return totVisit;
        }

        public int getTotDefect()
        {
            return totDefect;
        }
        
    }
}