﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarParkCheckList.Models
{

    public class ActivityLog
    {
        public String user { get; set; }
        public String datetime { get; set; }
        public String action { get; set; }
        public String timelapse { get; set; }
    }    
}