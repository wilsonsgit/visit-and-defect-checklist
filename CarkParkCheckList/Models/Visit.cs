﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarParkCheckList.Models
{
  
    public class Visit
    {
        public String jobid { get; set; }
        public String operationExec { get; set; }
        public String site { get; set; }
        public String date { get; set; }
        public String time { get; set; }
        public String carInPark { get; set; }
        public String intercom { get; set; }
        public String spareBarrierArm { get; set; }
        public String buildingTenancy { get; set; }
        public String equipmentCleanliness { get; set; }
        public String copBookNo { get; set; }
        public String seasonTerminations { get; set; }
        public String newSeasonTerminations { get; set; }
        public String refundForms { get; set; }
        public String cctvCamera { get; set; }
        public String recordingStatus { get; set; }
        public String timeSynPms { get; set; }
        public String earliestDateOfFootage { get; set; }
        public String totalBarrierRaise { get; set; }
        public String receiptChecks { get; set; }
        public String containersOnLoc { get; set; }
        public String abandVehicle { get; set; }
        public String surroundingChanges { get; set; }
        public String remarks { get; set; }
        public String createdby { get; set; }
        public String updatedby { get; set; }
        public String lastupdate { get; set; }

      

    }    
}