﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarParkCheckList.Models
{
  
    public class Defects
    {
        public String jobid { get; set; }
        public String operationExec { get; set; }
        public String site { get; set; }
        public String date { get; set; }
        public String time { get; set; }
        public String bollards { get; set; }
        public String equipmentCleanliness { get; set; }
        public String sign { get; set; }
        public String spotLight { get; set; }
        public String lightBox { get; set; }
        public String scrapbarrier { get; set; }
        public String safetyConcerns { get; set; }
        public String carParkLights { get; set; }
        public String potHoles { get; set; }
        public String generalCleanliness { get; set; }
        public String waterPounding { get; set; }
        public String waterPoundingcpl { get; set; }
        public String fadedPaintWorks { get; set; }
        public String oilLeak { get; set; }
        public String pgsSystem { get; set; }
        public String dmgSurface { get; set; }
        public String scanningIssues { get; set; }
        public String cardReader { get; set; }
        public String cctv { get; set; }
        public String systemKiosk { get; set; }
        public String barrierKiosk { get; set; }
        public String barrierPadding { get; set; }
        public String cableTie { get; set; }
        public String acpipe { get; set; }
        public String boothCondition { get; set; }
        public String boothFlooring { get; set; }
        public String boothWrap { get; set; }
        public String remarks { get; set; }
        public String createdby { get; set; }
        public String updatedby { get; set; }
        public String lastupdate { get; set; }

    }    
}