﻿using CarParkCheckList.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarkParkCheckList.Models
{
    public class UserSession
    {
        public string Token {get; set;}
        public Site site { get; set; }
        public string RequestBy { get; set; }
        public string RequestTime{ get; set; }
        public string TTL { get; set; }
        public string IP { get; set; }
    }
}