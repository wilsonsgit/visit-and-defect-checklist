﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarkParkCheckList.Models
{
    public class User
    {
        public String userid { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public String salt { get; set; }
        public String email { get; set; }
        public String datetimecreated { get; set; }
        public String lastdatetimelogin { get; set; }
        public String roleid { get; set; }
        public String islock { get; set; }
        public String createdby { get; set; }
    }
}